def retrieve_best_lang(dictionary):
    try:
        return dictionary['en']
    except KeyError:
        return next(iter(dictionary.values()))


def get_max_length(list_of_values):
    # Used mainly for obtaining the maximun length value from a list of model choices
    return max([len(item[0]) for item in list_of_values])
