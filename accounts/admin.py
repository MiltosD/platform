from django.contrib import admin

from accounts import models

admin.autodiscover()

admin.site.register(models.ELGUser)
