import pytest
from rest_framework.exceptions import ValidationError

from accounts.models import ELGUser

pytestmark = pytest.mark.django_db


@pytest.mark.django_db
class TestUserCreation:
    pytestmark = pytest.mark.django_db

    test_user = dict(username='test', password='test', email='test@test.com')

    def test_create_inactive_user(self):
        user = ELGUser.objects.create_user(**self.test_user)
        assert user.pk == 1
        assert user.is_active is False

    def test_create_missing_values(self):
        with pytest.raises(ValueError) as error:
            ELGUser.objects.create_user(username='test1', password='test')
            assert error.value == 'Please enter a valid email.'
        with pytest.raises(TypeError) as error:
            ELGUser.objects.create_user(password='test', email='test@test.com')
            assert error.value == 'Please enter a username'
        with pytest.raises(ValueError) as error:
            ELGUser.objects.create_user(username='test', email='test@test.com')
            assert error.value == 'Please enter a password'

    def test_create_check_duplicate_username(self):
        with pytest.raises(ValidationError) as error:
            ELGUser.objects.create_user(username='test1', password='test', email='test1@test.com')
            assert ELGUser.objects.count() == 1
            ELGUser.objects.create_user(username='test1', password='test', email='test2@test.com')
            assert error.value == 'This username already exists'
            assert ELGUser.objects.count() == 1

    def test_create_check_duplicate_email(self):
        with pytest.raises(ValidationError) as error:
            ELGUser.objects.create_user(username='test1', password='test', email='test1@test.com')
            assert ELGUser.objects.count() == 1
            ELGUser.objects.create_user(username='test2', password='test', email='test1@test.com')
            assert error.value == 'This email is already used'
            assert ELGUser.objects.count() == 1

    def test_create_superuser(self):
        user = ELGUser.objects.create_superuser(**self.test_user)
        user.is_superuser = True
        user.save()
        assert user.is_superuser is True
