import pytest

from django.contrib.auth.models import Group

pytestmark = pytest.mark.django_db


@pytest.mark.django_db
class TestGroup:
    pytestmark = pytest.mark.django_db

    def test_create_group(self):
        assert Group.objects.count() == 0
        Group.objects.create(name='test_group')
        assert Group.objects.count() == 1
