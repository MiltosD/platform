from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import AbstractUser, PermissionsMixin
from rest_framework.exceptions import ValidationError


class ELGUserManager(BaseUserManager):
    def create_user(self, username, email=None, password=None, **extra_fields):
        if not email:
            raise ValueError('Please enter a valid email.')
        if not username:
            raise ValueError('Please enter a username')
        if not password:
            raise ValueError('Please enter a password')
        user = self.model(
            email=self.normalize_email(email),
            username=username
        )
        if self.model.objects.filter(username=username).exists():
            raise ValidationError('This username already exists')
        elif self.model.objects.filter(email=email).exists():
            raise ValidationError('This email is already used')
        user.is_active = False
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, email, password, **extra_fields):
        user = self.create_user(username, email, password)
        user.active = True
        user.is_staff = True
        user.is_superuser = True
        user.save()
        return user


class ELGUser(AbstractUser):
    objects = ELGUserManager()
    pass
