def _create_uuid():
    """
    Creates a unique id from a UUID-1 and a UUID-4, checks for collisions.
    """
    # Create new identifier based on a UUID-1 and a UUID-4.
    new_id = '{0}{1}'.format(uuid1().hex, uuid4().hex)

    # Check for collisions; in case of a collision, create new identifier.
    while StorageObject.objects.filter(identifier=new_id):
        new_id = '{0}{1}'.format(uuid1().hex, uuid4().hex)