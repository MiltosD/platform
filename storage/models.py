from uuid import uuid4

from django.db import models


def _create_uuid():
    """
    Creates a unique id from a UUID-4, checks for collisions.
    """
    # Create new identifier based on a UUID-1 and a UUID-4.
    new_id = uuid4().hex

    # Check for collisions; in case of a collision, create new identifier.
    while StorageObject.objects.filter(identifier=new_id):
        new_id = uuid4().hex


# Create your models here.
class StorageObject(models.Model):
    """

    """

    class Meta:
        ordering = ('-pk',)

    resource_metadata_record = models.OneToOneField(
        'repository.ResourceMetadataRecord',
        related_name='storage_object',
    )

    identifier = models.CharField(
        max_length=64,
        default=_create_uuid,
        editable=False,
        unique=True,
        help_text="(Read-only) unique identifier for this storage object instance.")
