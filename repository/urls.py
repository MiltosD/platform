from django.urls import path, include
from rest_framework import routers
from rest_framework.renderers import CoreJSONRenderer
from rest_framework.schemas import get_schema_view

from elg_platform.settings.settings import DJANGO_URL
from repository.api import api, views

router = routers.DefaultRouter()
router.register('resourcemetadatarecord', api.ResourceMetadataRecordViewSet)
router.register('actor', api.ActorViewSet)
router.register('annotation', api.AnnotationViewSet)
router.register('annotationtype', api.AnnotationTypeViewSet)
router.register('characterencoding', api.CharacterEncodingViewSet)
router.register('commoninfo', api.CommonInfoViewSet)
router.register('componentdependency', api.ComponentDependencyViewSet)
router.register('componentdistribution', api.ComponentDistributionViewSet)
router.register('contact', api.ContactViewSet)

router.register('languageresource', api.LanguageResourceViewSet)
router.register('corpus', api.CorpusViewSet)
router.register('toolservice', api.ToolServiceViewSet)
router.register('toolservicecreation', api.ToolServiceCreationViewSet)

router.register('datasetdistribution', api.DatasetDistributionViewSet)
router.register('date', api.DateViewSet)
router.register('daterangetype', api.DateRangeTypeViewSet)
router.register('datecombination', api.DateCombinationViewSet)
router.register('distributionrights', api.DistributionRightsViewSet)
router.register('creation', api.CreationViewSet)
router.register('dataformat', api.DataFormatViewSet)
router.register('document', api.DocumentViewSet)
router.register('documentation', api.DocumentationViewSet)
router.register('domain', api.DomainViewSet)
router.register('functioninfo', api.FunctionInfoViewSet)
router.register('geographicclassification', api.GeographicClassificationViewSet)
router.register('group', api.GroupViewSet)
router.register('groupidentifier', api.GroupIdentifierViewSet)
router.register('identification', api.IdentificationViewSet)
router.register('language', api.LanguageViewSet)
router.register('languageinfo', api.LanguageInfoViewSet)
router.register('languagevariety', api.LanguageVarietyViewSet)
router.register('licence', api.LicenceViewSet)
router.register('licenceidentifier', api.LicenceIdentifierViewSet)
router.register('linguality', api.LingualityViewSet)
router.register('mailinglist', api.MailingListViewSet)
router.register('mediapart', api.MediaPartViewSet)
router.register('mediatechnicalinfo', api.MediaTechnicalInfoViewSet)
router.register('textdatasettechnicalinfo', api.TextDatasetTechnicalInfoViewSet)
router.register('metadataheader', api.MetadataHeaderViewSet)
router.register('metadataidentifier', api.MetadataIdentifierViewSet)
router.register('organization', api.OrganizationViewSet)
router.register('organizationidentifier', api.OrganizationIdentifierViewSet)
router.register('person', api.PersonViewSet)
router.register('personidentifier', api.PersonIdentifierViewSet)
router.register('physicalresource', api.PhysicalResourceViewSet)
router.register('modality', api.ModalityViewSet)
router.register('processingresource', api.ProcessingResourceViewSet)
router.register('publicationidentifier', api.PublicationIdentifierViewSet)
router.register('register', api.RegisterViewSet)
router.register('relation', api.RelationViewSet)
router.register('repository', api.RepositoryViewSet)
router.register('repositoryidentifier', api.RepositoryIdentifierViewSet)
router.register('resource', api.ResourceViewSet)
router.register('resourceidentifier', api.ResourceIdentifierViewSet)
router.register('resourcerights', api.ResourceRightsViewSet)
router.register('size', api.SizeViewSet)
router.register('textformat', api.TextFormatViewSet)
router.register('textpart', api.TextPartViewSet)
router.register('timeclassification', api.TimeClassificationViewSet)
router.register('sourceofmetadatarecord', api.SourceOfMetadataRecordViewSet)
router.register('subject', api.SubjectViewSet)
router.register('textclassification', api.TextClassificationViewSet)
router.register('textgenre', api.TextGenreViewSet)
router.register('texttype', api.TextTypeViewSet)
router.register('version', api.VersionViewSet)

app_name = 'repository'

urlpatterns = (
    path('', include(router.urls)),
)

schema_view = get_schema_view(
    title='ELG Backend API',
    url=f'{DJANGO_URL}/api/',
    renderer_classes=[CoreJSONRenderer]
)

urlpatterns += (
    path('schema/', schema_view),
    # urls for ResourceMetadataRecord
    path('resourcemetadatarecord/', views.ResourceMetadataRecordListView.as_view(),
         name='resourcemetadatarecord_list'),
    path('resourcemetadatarecord/create/', views.ResourceMetadataRecordCreateView.as_view(),
         name='resourcemetadatarecord_create'),
    path('resourcemetadatarecord/detail/<int:pk>/', views.ResourceMetadataRecordDetailView.as_view(),
         name='resourcemetadatarecord_detail'),
    path('resourcemetadatarecord/update/<int:pk>/', views.ResourceMetadataRecordUpdateView.as_view(),
         name='resourcemetadatarecord_update'),
)

urlpatterns += (
    # urls for Actor
    path('actor/', views.ActorListView.as_view(), name='actor_list'),
    path('actor/create/', views.ActorCreateView.as_view(), name='actor_create'),
    path('actor/detail/<int:pk>/', views.ActorDetailView.as_view(), name='actor_detail'),
    path('actor/update/<int:pk>/', views.ActorUpdateView.as_view(), name='actor_update'),
)

urlpatterns += (
    # urls for Annotation
    path('annotation/', views.AnnotationListView.as_view(), name='annotation_list'),
    path('annotation/create/', views.AnnotationCreateView.as_view(), name='annotation_create'),
    path('annotation/detail/<int:pk>/', views.AnnotationDetailView.as_view(),
         name='annotation_detail'),
    path('annotation/update/<int:pk>/', views.AnnotationUpdateView.as_view(),
         name='annotation_update'),
)

urlpatterns += (
    # urls for AnnotationType
    path('annotationtype/', views.AnnotationTypeListView.as_view(), name='annotationtype_list'),
    path('annotationtype/create/', views.AnnotationTypeCreateView.as_view(),
         name='annotationtype_create'),
    path('annotationtype/detail/<int:pk>/', views.AnnotationTypeDetailView.as_view(),
         name='annotationtype_detail'),
    path('annotationtype/update/<int:pk>/', views.AnnotationTypeUpdateView.as_view(),
         name='annotationtype_update'),
)

urlpatterns += (
    # urls for CharacterEncoding
    path('characterencoding/', views.CharacterEncodingListView.as_view(),
         name='characterencoding_list'),
    path('characterencoding/create/', views.CharacterEncodingCreateView.as_view(),
         name='characterencoding_create'),
    path('characterencoding/detail/<int:pk>/', views.CharacterEncodingDetailView.as_view(),
         name='characterencoding_detail'),
    path('characterencoding/update/<int:pk>/', views.CharacterEncodingUpdateView.as_view(),
         name='characterencoding_update'),
)

urlpatterns += (
    # urls for CommonInfo
    path('commoninfo/', views.CommonInfoListView.as_view(), name='commoninfo_list'),
    path('commoninfo/create/', views.CommonInfoCreateView.as_view(), name='commoninfo_create'),
    path('commoninfo/detail/<int:pk>/', views.CommonInfoDetailView.as_view(),
         name='commoninfo_detail'),
    path('commoninfo/update/<int:pk>/', views.CommonInfoUpdateView.as_view(),
         name='commoninfo_update'),
)

urlpatterns += (
    # urls for ComponentDependency
    path('componentdependency/', views.ComponentDependencyListView.as_view(),
         name='componentdependency_list'),
    path('componentdependency/create/', views.ComponentDependencyCreateView.as_view(),
         name='componentdependency_create'),
    path('componentdependency/detail/<int:pk>/', views.ComponentDependencyDetailView.as_view(),
         name='componentdependency_detail'),
    path('componentdependency/update/<int:pk>/', views.ComponentDependencyUpdateView.as_view(),
         name='componentdependency_update'),
)

urlpatterns += (
    # urls for ComponentDistribution
    path('componentdistribution/', views.ComponentDistributionListView.as_view(),
         name='componentdistribution_list'),
    path('componentdistribution/create/', views.ComponentDistributionCreateView.as_view(),
         name='componentdistribution_create'),
    path('componentdistribution/detail/<int:pk>/', views.ComponentDistributionDetailView.as_view(),
         name='componentdistribution_detail'),
    path('componentdistribution/update/<int:pk>/', views.ComponentDistributionUpdateView.as_view(),
         name='componentdistribution_update'),
)

urlpatterns += (
    # urls for Contact
    path('contact/', views.ContactListView.as_view(), name='contact_list'),
    path('contact/create/', views.ContactCreateView.as_view(), name='contact_create'),
    path('contact/detail/<int:pk>/', views.ContactDetailView.as_view(), name='contact_detail'),
    path('contact/update/<int:pk>/', views.ContactUpdateView.as_view(), name='contact_update'),
)

urlpatterns += (
    # urls for LanguageResource
    path('languageresource/', views.LanguageResourceListView.as_view(),
         name='languageresource_list'),
    path('languageresource/create/', views.LanguageResourceCreateView.as_view(),
         name='languageresource_create'),
    path('languageresource/detail/<int:pk>/', views.LanguageResourceDetailView.as_view(),
         name='languageresource_detail'),
    path('languageresource/update/<int:pk>/', views.LanguageResourceUpdateView.as_view(),
         name='languageresource_update'),
)

urlpatterns += (
    # urls for DatasetDistribution
    path('datasetdistribution/', views.DatasetDistributionListView.as_view(),
         name='datasetdistribution_list'),
    path('datasetdistribution/create/', views.DatasetDistributionCreateView.as_view(),
         name='datasetdistribution_create'),
    path('datasetdistribution/detail/<int:pk>/', views.DatasetDistributionDetailView.as_view(),
         name='datasetdistribution_detail'),
    path('datasetdistribution/update/<int:pk>/', views.DatasetDistributionUpdateView.as_view(),
         name='datasetdistribution_update'),
)

urlpatterns += (
    # urls for Date
    path('date/', views.DateListView.as_view(), name='date_list'),
    path('date/create/', views.DateCreateView.as_view(), name='date_create'),
    path('date/detail/<int:pk>/', views.DateDetailView.as_view(), name='date_detail'),
    path('date/update/<int:pk>/', views.DateUpdateView.as_view(), name='date_update'),
)

urlpatterns += (
    # urls for DateRangeType
    path('daterangetype/', views.DateRangeTypeListView.as_view(), name='daterangetype_list'),
    path('daterangetype/create/', views.DateRangeTypeCreateView.as_view(),
         name='daterangetype_create'),
    path('daterangetype/detail/<int:pk>/', views.DateRangeTypeDetailView.as_view(),
         name='daterangetype_detail'),
    path('daterangetype/update/<int:pk>/', views.DateRangeTypeUpdateView.as_view(),
         name='daterangetype_update'),
)

urlpatterns += (
    # urls for DateCombination
    path('datecombination/', views.DateCombinationListView.as_view(),
         name='datecombination_list'),
    path('datecombination/create/', views.DateCombinationCreateView.as_view(),
         name='datecombination_create'),
    path('datecombination/detail/<int:pk>/', views.DateCombinationDetailView.as_view(),
         name='datecombination_detail'),
    path('datecombination/update/<int:pk>/', views.DateCombinationUpdateView.as_view(),
         name='datecombination_update'),
)

urlpatterns += (
    # urls for DistributionRights
    path('distributionrights/', views.DistributionRightsListView.as_view(),
         name='distributionrights_list'),
    path('distributionrights/create/', views.DistributionRightsCreateView.as_view(),
         name='distributionrights_create'),
    path('distributionrights/detail/<int:pk>/', views.DistributionRightsDetailView.as_view(),
         name='distributionrights_detail'),
    path('distributionrights/update/<int:pk>/', views.DistributionRightsUpdateView.as_view(),
         name='distributionrights_update'),
)

urlpatterns += (
    # urls for Creation
    path('creation/', views.CreationListView.as_view(), name='creation_list'),
    path('creation/create/', views.CreationCreateView.as_view(), name='creation_create'),
    path('creation/detail/<int:pk>/', views.CreationDetailView.as_view(), name='creation_detail'),
    path('creation/update/<int:pk>/', views.CreationUpdateView.as_view(), name='creation_update'),
)

urlpatterns += (
    # urls for DataFormat
    path('dataformat/', views.DataFormatListView.as_view(), name='dataformat_list'),
    path('dataformat/create/', views.DataFormatCreateView.as_view(), name='dataformat_create'),
    path('dataformat/detail/<int:pk>/', views.DataFormatDetailView.as_view(),
         name='dataformat_detail'),
    path('dataformat/update/<int:pk>/', views.DataFormatUpdateView.as_view(),
         name='dataformat_update'),
)

urlpatterns += (
    # urls for Document
    path('document/', views.DocumentListView.as_view(), name='document_list'),
    path('document/create/', views.DocumentCreateView.as_view(), name='document_create'),
    path('document/detail/<int:pk>/', views.DocumentDetailView.as_view(), name='document_detail'),
    path('document/update/<int:pk>/', views.DocumentUpdateView.as_view(), name='document_update'),
)

urlpatterns += (
    # urls for Document
    path('documentation/', views.DocumentationListView.as_view(), name='documentation_list'),
    path('domain/', views.DomainListView.as_view(), name='domain_list'),
)

urlpatterns += (
    # urls for FunctionInfo
    path('functioninfo/', views.FunctionInfoListView.as_view(), name='functioninfo_list'),
    path('functioninfo/create/', views.FunctionInfoCreateView.as_view(),
         name='functioninfo_create'),
    path('functioninfo/detail/<int:pk>/', views.FunctionInfoDetailView.as_view(),
         name='functioninfo_detail'),
    path('functioninfo/update/<int:pk>/', views.FunctionInfoUpdateView.as_view(),
         name='functioninfo_update'),
)

urlpatterns += (
    # urls for GeographicClassification
    path('geographicclassification/', views.GeographicClassificationListView.as_view(),
         name='geographicclassification_list'),
    path('geographicclassification/create/', views.GeographicClassificationCreateView.as_view(),
         name='geographicclassification_create'),
    path('geographicclassification/detail/<int:pk>/', views.GeographicClassificationDetailView.as_view(),
         name='geographicclassification_detail'),
    path('geographicclassification/update/<int:pk>/', views.GeographicClassificationUpdateView.as_view(),
         name='geographicclassification_update'),
)

urlpatterns += (
    # urls for GroupIdentifier
    path('groupidentifier/', views.GroupIdentifierListView.as_view(),
         name='groupidentifier_list'),
    path('groupidentifier/create/', views.GroupIdentifierCreateView.as_view(),
         name='groupidentifier_create'),
    path('groupidentifier/detail/<int:pk>/', views.GroupIdentifierDetailView.as_view(),
         name='groupidentifier_detail'),
    path('groupidentifier/update/<int:pk>/', views.GroupIdentifierUpdateView.as_view(),
         name='groupidentifier_update'),
)

urlpatterns += (
    # urls for Identification
    path('identification/', views.IdentificationListView.as_view(), name='identification_list'),
    path('identification/create/', views.IdentificationCreateView.as_view(),
         name='identification_create'),
    path('identification/detail/<int:pk>/', views.IdentificationDetailView.as_view(),
         name='identification_detail'),
    path('identification/update/<int:pk>/', views.IdentificationUpdateView.as_view(),
         name='identification_update'),
)

urlpatterns += (
    # urls for Language
    path('language/', views.LanguageListView.as_view(), name='language_list'),
    path('language/create/', views.LanguageCreateView.as_view(), name='language_create'),
    path('language/detail/<int:pk>/', views.LanguageDetailView.as_view(), name='language_detail'),
    path('language/update/<int:pk>/', views.LanguageUpdateView.as_view(), name='language_update'),
)

urlpatterns += (
    # urls for LanguageInfo
    path('languageinfo/', views.LanguageInfoListView.as_view(), name='languageinfo_list'),
    path('languageinfo/create/', views.LanguageInfoCreateView.as_view(),
         name='languageinfo_create'),
    path('languageinfo/detail/<int:pk>/', views.LanguageInfoDetailView.as_view(),
         name='languageinfo_detail'),
    path('languageinfo/update/<int:pk>/', views.LanguageInfoUpdateView.as_view(),
         name='languageinfo_update'),
)

urlpatterns += (
    # urls for LanguageVariety
    path('languagevariety/', views.LanguageVarietyListView.as_view(),
         name='languagevariety_list'),
    path('languagevariety/create/', views.LanguageVarietyCreateView.as_view(),
         name='languagevariety_create'),
    path('languagevariety/detail/<int:pk>/', views.LanguageVarietyDetailView.as_view(),
         name='languagevariety_detail'),
    path('languagevariety/update/<int:pk>/', views.LanguageVarietyUpdateView.as_view(),
         name='languagevariety_update'),
)

urlpatterns += (
    # urls for Licence
    path('licence/', views.LicenceListView.as_view(), name='licence_list'),
    path('licence/create/', views.LicenceCreateView.as_view(), name='licence_create'),
    path('licence/detail/<int:pk>/', views.LicenceDetailView.as_view(), name='licence_detail'),
    path('licence/update/<int:pk>/', views.LicenceUpdateView.as_view(), name='licence_update'),
)

urlpatterns += (
    # urls for LicenceIdentifier
    path('licenceidentifier/', views.LicenceIdentifierListView.as_view(),
         name='licenceidentifier_list'),
    path('licenceidentifier/create/', views.LicenceIdentifierCreateView.as_view(),
         name='licenceidentifier_create'),
    path('licenceidentifier/detail/<int:pk>/', views.LicenceIdentifierDetailView.as_view(),
         name='licenceidentifier_detail'),
    path('licenceidentifier/update/<int:pk>/', views.LicenceIdentifierUpdateView.as_view(),
         name='licenceidentifier_update'),
)

urlpatterns += (
    # urls for Linguality
    path('linguality/', views.LingualityListView.as_view(), name='linguality_list'),
    path('linguality/create/', views.LingualityCreateView.as_view(), name='linguality_create'),
    path('linguality/detail/<int:pk>/', views.LingualityDetailView.as_view(),
         name='linguality_detail'),
    path('linguality/update/<int:pk>/', views.LingualityUpdateView.as_view(),
         name='linguality_update'),
)

urlpatterns += (
    # urls for MailingList
    path('mailinglist/', views.MailingListListView.as_view(), name='mailinglist_list'),
    path('mailinglist/create/', views.MailingListCreateView.as_view(), name='mailinglist_create'),
    path('mailinglist/detail/<int:pk>/', views.MailingListDetailView.as_view(),
         name='mailinglist_detail'),
    path('mailinglist/update/<int:pk>/', views.MailingListUpdateView.as_view(),
         name='mailinglist_update'),
)

urlpatterns += (
    # urls for MediaPart
    path('mediapart/', views.MediaPartListView.as_view(), name='mediapart_list'),
    path('mediapart/create/', views.MediaPartCreateView.as_view(), name='mediapart_create'),
    path('mediapart/detail/<int:pk>/', views.MediaPartDetailView.as_view(),
         name='mediapart_detail'),
    path('mediapart/update/<int:pk>/', views.MediaPartUpdateView.as_view(),
         name='mediapart_update'),
)

urlpatterns += (
    # urls for MediaTechnicalInfo
    path('mediatechnicalinfo/', views.MediaTechnicalInfoListView.as_view(),
         name='mediatechnicalinfo_list'),
    path('mediatechnicalinfo/create/', views.MediaTechnicalInfoCreateView.as_view(),
         name='mediatechnicalinfo_create'),
    path('mediatechnicalinfo/detail/<int:pk>/', views.MediaTechnicalInfoDetailView.as_view(),
         name='mediatechnicalinfo_detail'),
    path('mediatechnicalinfo/update/<int:pk>/', views.MediaTechnicalInfoUpdateView.as_view(),
         name='mediatechnicalinfo_update'),
)

urlpatterns += (
    # urls for MetadataHeader
    path('metadataheader/', views.MetadataHeaderListView.as_view(), name='metadataheader_list'),
    path('metadataheader/create/', views.MetadataHeaderCreateView.as_view(),
         name='metadataheader_create'),
    path('metadataheader/detail/<int:pk>/', views.MetadataHeaderDetailView.as_view(),
         name='metadataheader_detail'),
    path('metadataheader/update/<int:pk>/', views.MetadataHeaderUpdateView.as_view(),
         name='metadataheader_update'),
)

urlpatterns += (
    # urls for MetadataIdentifier
    path('metadataidentifier/', views.MetadataIdentifierListView.as_view(),
         name='metadataidentifier_list'),
    path('metadataidentifier/create/', views.MetadataIdentifierCreateView.as_view(),
         name='metadataidentifier_create'),
    path('metadataidentifier/detail/<int:pk>/', views.MetadataIdentifierDetailView.as_view(),
         name='metadataidentifier_detail'),
    path('metadataidentifier/update/<int:pk>/', views.MetadataIdentifierUpdateView.as_view(),
         name='metadataidentifier_update'),
)

urlpatterns += (
    # urls for OrganizationIdentifier
    path('organizationidentifier/', views.OrganizationIdentifierListView.as_view(),
         name='organizationidentifier_list'),
    path('organizationidentifier/create/', views.OrganizationIdentifierCreateView.as_view(),
         name='organizationidentifier_create'),
    path('organizationidentifier/detail/<int:pk>/', views.OrganizationIdentifierDetailView.as_view(),
         name='organizationidentifier_detail'),
    path('organizationidentifier/update/<int:pk>/', views.OrganizationIdentifierUpdateView.as_view(),
         name='organizationidentifier_update'),
)

urlpatterns += (
    # urls for PersonIdentifier
    path('personidentifier/', views.PersonIdentifierListView.as_view(),
         name='personidentifier_list'),
    path('personidentifier/create/', views.PersonIdentifierCreateView.as_view(),
         name='personidentifier_create'),
    path('personidentifier/detail/<int:pk>/', views.PersonIdentifierDetailView.as_view(),
         name='personidentifier_detail'),
    path('personidentifier/update/<int:pk>/', views.PersonIdentifierUpdateView.as_view(),
         name='personidentifier_update'),
)

urlpatterns += (
    # urls for PhysicalResource
    path('physicalresource/', views.PhysicalResourceListView.as_view(),
         name='physicalresource_list'),
    path('physicalresource/create/', views.PhysicalResourceCreateView.as_view(),
         name='physicalresource_create'),
    path('physicalresource/detail/<int:pk>/', views.PhysicalResourceDetailView.as_view(),
         name='physicalresource_detail'),
    path('physicalresource/update/<int:pk>/', views.PhysicalResourceUpdateView.as_view(),
         name='physicalresource_update'),
)

urlpatterns += (
    # urls for Modality
    path('modality/', views.ModalityListView.as_view(), name='modality_list'),
    path('modality/create/', views.ModalityCreateView.as_view(), name='modality_create'),
    path('modality/detail/<int:pk>/', views.ModalityDetailView.as_view(), name='modality_detail'),
    path('modality/update/<int:pk>/', views.ModalityUpdateView.as_view(), name='modality_update'),
)

urlpatterns += (
    # urls for ProcessingResource
    path('processingresource/', views.ProcessingResourceListView.as_view(),
         name='processingresource_list'),
    path('processingresource/create/', views.ProcessingResourceCreateView.as_view(),
         name='processingresource_create'),
    path('processingresource/detail/<int:pk>/', views.ProcessingResourceDetailView.as_view(),
         name='processingresource_detail'),
    path('processingresource/update/<int:pk>/', views.ProcessingResourceUpdateView.as_view(),
         name='processingresource_update'),
)

urlpatterns += (
    # urls for PublicationIdentifier
    path('publicationidentifier/', views.PublicationIdentifierListView.as_view(),
         name='publicationidentifier_list'),
    path('publicationidentifier/create/', views.PublicationIdentifierCreateView.as_view(),
         name='publicationidentifier_create'),
    path('publicationidentifier/detail/<int:pk>/', views.PublicationIdentifierDetailView.as_view(),
         name='publicationidentifier_detail'),
    path('publicationidentifier/update/<int:pk>/', views.PublicationIdentifierUpdateView.as_view(),
         name='publicationidentifier_update'),
)

urlpatterns += (
    # urls for Register
    path('register/', views.RegisterListView.as_view(), name='register_list'),
    path('register/create/', views.RegisterCreateView.as_view(), name='register_create'),
    path('register/detail/<int:pk>/', views.RegisterDetailView.as_view(), name='register_detail'),
    path('register/update/<int:pk>/', views.RegisterUpdateView.as_view(), name='register_update'),
)

urlpatterns += (
    # urls for Repository
    path('', views.RepositoryListView.as_view(), name='repository_list'),
    path('create/', views.RepositoryCreateView.as_view(), name='repository_create'),
    path('detail/<int:pk>/', views.RepositoryDetailView.as_view(),
         name='repository_detail'),
    path('update/<int:pk>/', views.RepositoryUpdateView.as_view(),
         name='repository_update'),
)

urlpatterns += (
    # urls for RepositoryIdentifier
    path('repositoryidentifier/', views.RepositoryIdentifierListView.as_view(),
         name='repositoryidentifier_list'),
    path('repositoryidentifier/create/', views.RepositoryIdentifierCreateView.as_view(),
         name='repositoryidentifier_create'),
    path('repositoryidentifier/detail/<int:pk>/', views.RepositoryIdentifierDetailView.as_view(),
         name='repositoryidentifier_detail'),
    path('repositoryidentifier/update/<int:pk>/', views.RepositoryIdentifierUpdateView.as_view(),
         name='repositoryidentifier_update'),
)

urlpatterns += (
    # urls for Resource
    path('resource/', views.ResourceListView.as_view(), name='resource_list'),
    path('resource/create/', views.ResourceCreateView.as_view(), name='resource_create'),
    path('resource/detail/<int:pk>/', views.ResourceDetailView.as_view(), name='resource_detail'),
    path('resource/update/<int:pk>/', views.ResourceUpdateView.as_view(), name='resource_update'),
)

urlpatterns += (
    # urls for ResourceIdentifier
    path('resourceidentifier/', views.ResourceIdentifierListView.as_view(),
         name='resourceidentifier_list'),
    path('resourceidentifier/create/', views.ResourceIdentifierCreateView.as_view(),
         name='resourceidentifier_create'),
    path('resourceidentifier/detail/<int:pk>/', views.ResourceIdentifierDetailView.as_view(),
         name='resourceidentifier_detail'),
    path('resourceidentifier/update/<int:pk>/', views.ResourceIdentifierUpdateView.as_view(),
         name='resourceidentifier_update'),
)

urlpatterns += (
    # urls for ResourceRights
    path('resourcerights/', views.ResourceRightsListView.as_view(), name='resourcerights_list'),
    path('resourcerights/create/', views.ResourceRightsCreateView.as_view(),
         name='resourcerights_create'),
    path('resourcerights/detail/<int:pk>/', views.ResourceRightsDetailView.as_view(),
         name='resourcerights_detail'),
    path('resourcerights/update/<int:pk>/', views.ResourceRightsUpdateView.as_view(),
         name='resourcerights_update'),
)

urlpatterns += (
    # urls for Size
    path('size/', views.SizeListView.as_view(), name='size_list'),
    path('size/create/', views.SizeCreateView.as_view(), name='size_create'),
    path('size/detail/<int:pk>/', views.SizeDetailView.as_view(), name='size_detail'),
    path('size/update/<int:pk>/', views.SizeUpdateView.as_view(), name='size_update'),
)

urlpatterns += (
    # urls for TextFormat
    path('textformat/', views.TextFormatListView.as_view(), name='textformat_list'),
    path('textformat/create/', views.TextFormatCreateView.as_view(), name='textformat_create'),
    path('textformat/detail/<int:pk>/', views.TextFormatDetailView.as_view(),
         name='textformat_detail'),
    path('textformat/update/<int:pk>/', views.TextFormatUpdateView.as_view(),
         name='textformat_update'),
)

urlpatterns += (
    # urls for TextPart
    path('textpart/', views.TextPartListView.as_view(), name='textpart_list'),
    path('textpart/create/', views.TextPartCreateView.as_view(), name='textpart_create'),
    path('textpart/detail/<int:pk>/', views.TextPartDetailView.as_view(), name='textpart_detail'),
    path('textpart/update/<int:pk>/', views.TextPartUpdateView.as_view(), name='textpart_update'),
)

urlpatterns += (
    # urls for TimeClassification
    path('timeclassification/', views.TimeClassificationListView.as_view(),
         name='timeclassification_list'),
    path('timeclassification/create/', views.TimeClassificationCreateView.as_view(),
         name='timeclassification_create'),
    path('timeclassification/detail/<int:pk>/', views.TimeClassificationDetailView.as_view(),
         name='timeclassification_detail'),
    path('timeclassification/update/<int:pk>/', views.TimeClassificationUpdateView.as_view(),
         name='timeclassification_update'),
)

urlpatterns += (
    # urls for SourceOfMetadataRecord
    path('sourceofmetadatarecord/', views.SourceOfMetadataRecordListView.as_view(),
         name='sourceofmetadatarecord_list'),
    path('sourceofmetadatarecord/create/', views.SourceOfMetadataRecordCreateView.as_view(),
         name='sourceofmetadatarecord_create'),
    path('sourceofmetadatarecord/detail/<int:pk>/', views.SourceOfMetadataRecordDetailView.as_view(),
         name='sourceofmetadatarecord_detail'),
    path('sourceofmetadatarecord/update/<int:pk>/', views.SourceOfMetadataRecordUpdateView.as_view(),
         name='sourceofmetadatarecord_update'),
)

urlpatterns += (
    # urls for Subject
    path('subject/', views.SubjectListView.as_view(), name='subject_list'),
    path('subject/create/', views.SubjectCreateView.as_view(), name='subject_create'),
    path('subject/detail/<int:pk>/', views.SubjectDetailView.as_view(), name='subject_detail'),
    path('subject/update/<int:pk>/', views.SubjectUpdateView.as_view(), name='subject_update'),
)

urlpatterns += (
    # urls for TextClassification
    path('textclassification/', views.TextClassificationListView.as_view(),
         name='textclassification_list'),
    path('textclassification/create/', views.TextClassificationCreateView.as_view(),
         name='textclassification_create'),
    path('textclassification/detail/<int:pk>/', views.TextClassificationDetailView.as_view(),
         name='textclassification_detail'),
    path('textclassification/update/<int:pk>/', views.TextClassificationUpdateView.as_view(),
         name='textclassification_update'),
)

urlpatterns += (
    # urls for TextGenre
    path('textgenre/', views.TextGenreListView.as_view(), name='textgenre_list'),
    path('textgenre/create/', views.TextGenreCreateView.as_view(), name='textgenre_create'),
    path('textgenre/detail/<int:pk>/', views.TextGenreDetailView.as_view(),
         name='textgenre_detail'),
    path('textgenre/update/<int:pk>/', views.TextGenreUpdateView.as_view(),
         name='textgenre_update'),
)

urlpatterns += (
    # urls for TextType
    path('texttype/', views.TextTypeListView.as_view(), name='texttype_list'),
    path('texttype/create/', views.TextTypeCreateView.as_view(), name='texttype_create'),
    path('texttype/detail/<int:pk>/', views.TextTypeDetailView.as_view(), name='texttype_detail'),
    path('texttype/update/<int:pk>/', views.TextTypeUpdateView.as_view(), name='texttype_update'),
)

urlpatterns += (
    # urls for Version
    path('version/', views.VersionListView.as_view(), name='version_list'),
    path('version/create/', views.VersionCreateView.as_view(), name='version_create'),
    path('version/detail/<int:pk>/', views.VersionDetailView.as_view(), name='version_detail'),
    path('version/update/<int:pk>/', views.VersionUpdateView.as_view(), name='version_update'),
)
