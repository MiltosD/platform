from django.conf import settings
from django_elasticsearch_dsl import DocType, Index, fields
from elasticsearch_dsl import analyzer

from repository.models import ResourceMetadataRecord

# Name of the Elasticsearch index
INDEX = Index(settings.ELASTICSEARCH_INDEX_NAMES[__name__])

# See Elasticsearch Indices API reference for available settings
INDEX.settings(
    number_of_shards=1,
    number_of_replicas=1
)

html_strip = analyzer(
    'html_strip',
    tokenizer="standard",
    filter=["standard", "lowercase", "stop", "snowball"],
    char_filter=["html_strip"]
)

@INDEX.doc_type
class ResourceMetadataRecordDocument(DocType):
    """ResourceMetadataRecord Elasticsearch document."""

    id = fields.IntegerField(attr='id')


    tags = fields.StringField(
        attr='name_indexing',
        analyzer=html_strip,
        fields={
            'raw': fields.StringField(analyzer='keyword', multi=True),
            'suggest': fields.CompletionField(multi=True),
        }
    )

    class Meta(object):
        """Meta options."""

        model = ResourceMetadataRecord  # The model associate with this DocType
