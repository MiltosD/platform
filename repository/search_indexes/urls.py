from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .viewsets.resource_metadata_record import ResourceMetadataRecordDocumentView


router = DefaultRouter()
metadata_records = router.register(r'metadata_records',
                        ResourceMetadataRecordDocumentView,
                        base_name='resourcemetadatarecorddocument')


urlpatterns = [
    path('', include(router.urls)),
]
