import json
import logging

import django
import pytest
from django.test import TestCase
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from accounts.tests.utils import create_users

API_ENDPOINT = '/api/repository/'
client = APIClient()

pytestmark = pytest.mark.django_db

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()

app_models = [m for m in django.apps.apps.get_app_config('repository').get_models()]


class TestModelsCreate(TestCase):

    def setUp(self):
        self.users = create_users()

    data = {
        "scheme_name": "Handle",
        "scheme_uri": "",
        "value": "some value"
    }

    def test_create_with_inactive(self):
        client.force_authenticate(user=self.users.get('inactive'))

        for model in app_models:
            label = model._meta.label_lower.replace('repository.', '')
            logger.info(f'Testing api creation: {model._meta.label}')
            response = client.post(
                reverse(f'repository:{label}_list'),
                data=json.dumps(self.data),
                content_type='application/json'
            )
            self.assertEqual(response.status_code, 401)

    def test_create_with_active(self):
        client.force_authenticate(user=self.users.get('active'))

        response = client.post(
            reverse('repository:metadataidentifier_list'),
            data=json.dumps(self.data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 401)

    def test_create_with_staff(self):
        client.force_authenticate(user=self.users.get('staff'))

        response = client.post(
            reverse('repository:metadataidentifier_list'),
            data=json.dumps(self.data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)

    def test_create_with_superuser(self):
        client.force_authenticate(user=self.users.get('admin'))

        response = client.post(
            reverse('repository:metadataidentifier_list'),
            data=json.dumps(self.data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)

    def test_create_with_invalid_value(self):
        data = self.data.copy()
        data['scheme_name'] = 'wrong'

        client.force_authenticate(user=self.admin)

        response = client.post(
            reverse('repository:metadataidentifier_list'),
            data=json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 401)
