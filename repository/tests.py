import unittest

from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.test import Client
from django.urls import reverse

from accounts.models import ELGUser
from .models import ResourceMetadataRecord, Actor, Annotation, AnnotationType, CharacterEncoding, CommonInfo, \
    ComponentDependency, ComponentDistribution, Contact, LanguageResource, DatasetDistribution, Date, DateRangeType, \
    DateCombination, DistributionRights, Creation, DataFormat, Document, FunctionInfo, GeographicClassification, \
    GroupIdentifier, Identification, Language, LanguageInfo, LanguageVariety, Licence, LicenceIdentifier, Linguality, \
    MailingList, MediaPart, MediaTechnicalInfo, MetadataHeader, MetadataIdentifier, OrganizationIdentifier, \
    PersonIdentifier, PhysicalResource, Modality, ProcessingResource, PublicationIdentifier, Register, Repository, \
    RepositoryIdentifier, Resource, ResourceIdentifier, ResourceRights, Size, TextFormat, TextPart, TimeClassification, \
    SourceOfMetadataRecord, Subject, TextClassification, TextGenre, TextType, Version


def create_django_contrib_auth_models_user(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return ELGUser.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_resourcemetadatarecord(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ResourceMetadataRecord.objects.create(**defaults)


def create_actor(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Actor.objects.create(**defaults)


def create_annotation(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Annotation.objects.create(**defaults)


def create_annotationtype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return AnnotationType.objects.create(**defaults)


def create_characterencoding(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return CharacterEncoding.objects.create(**defaults)


def create_commoninfo(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return CommonInfo.objects.create(**defaults)


def create_componentdependency(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ComponentDependency.objects.create(**defaults)


def create_componentdistribution(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ComponentDistribution.objects.create(**defaults)


def create_contact(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Contact.objects.create(**defaults)


def create_languageresource(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return LanguageResource.objects.create(**defaults)


def create_datasetdistribution(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return DatasetDistribution.objects.create(**defaults)


def create_date(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Date.objects.create(**defaults)


def create_daterangetype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return DateRangeType.objects.create(**defaults)


def create_datecombination(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return DateCombination.objects.create(**defaults)


def create_distributionrights(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return DistributionRights.objects.create(**defaults)


def create_creation(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Creation.objects.create(**defaults)


def create_dataformat(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return DataFormat.objects.create(**defaults)


def create_document(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Document.objects.create(**defaults)


def create_functioninfo(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return FunctionInfo.objects.create(**defaults)


def create_geographicclassification(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return GeographicClassification.objects.create(**defaults)


def create_groupidentifier(**kwargs):
    defaults = {}
    defaults["value"] = "value"
    defaults.update(**kwargs)
    return GroupIdentifier.objects.create(**defaults)


def create_identification(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Identification.objects.create(**defaults)


def create_language(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Language.objects.create(**defaults)


def create_languageinfo(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return LanguageInfo.objects.create(**defaults)


def create_languagevariety(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return LanguageVariety.objects.create(**defaults)


def create_licence(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Licence.objects.create(**defaults)


def create_licenceidentifier(**kwargs):
    defaults = {}
    defaults["value"] = "value"
    defaults.update(**kwargs)
    return LicenceIdentifier.objects.create(**defaults)


def create_linguality(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Linguality.objects.create(**defaults)


def create_mailinglist(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return MailingList.objects.create(**defaults)


def create_mediapart(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return MediaPart.objects.create(**defaults)


def create_mediatechnicalinfo(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return MediaTechnicalInfo.objects.create(**defaults)


def create_metadataheader(**kwargs):
    defaults = {}
    defaults["revision"] = "revision"
    defaults.update(**kwargs)
    return MetadataHeader.objects.create(**defaults)


def create_metadataidentifier(**kwargs):
    defaults = {}
    defaults["value"] = "value"
    defaults.update(**kwargs)
    return MetadataIdentifier.objects.create(**defaults)


def create_organizationidentifier(**kwargs):
    defaults = {}
    defaults["value"] = "value"
    defaults.update(**kwargs)
    return OrganizationIdentifier.objects.create(**defaults)


def create_personidentifier(**kwargs):
    defaults = {}
    defaults["value"] = "value"
    defaults.update(**kwargs)
    return PersonIdentifier.objects.create(**defaults)


def create_physicalresource(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return PhysicalResource.objects.create(**defaults)


def create_modality(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Modality.objects.create(**defaults)


def create_processingresource(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ProcessingResource.objects.create(**defaults)


def create_publicationidentifier(**kwargs):
    defaults = {}
    defaults["value"] = "value"
    defaults.update(**kwargs)
    return PublicationIdentifier.objects.create(**defaults)


def create_register(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Register.objects.create(**defaults)


def create_repository(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Repository.objects.create(**defaults)


def create_repositoryidentifier(**kwargs):
    defaults = {}
    defaults["value"] = "value"
    defaults.update(**kwargs)
    return RepositoryIdentifier.objects.create(**defaults)


def create_resource(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Resource.objects.create(**defaults)


def create_resourceidentifier(**kwargs):
    defaults = {}
    defaults["value"] = "value"
    defaults.update(**kwargs)
    return ResourceIdentifier.objects.create(**defaults)


def create_resourcerights(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ResourceRights.objects.create(**defaults)


def create_size(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Size.objects.create(**defaults)


def create_textformat(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return TextFormat.objects.create(**defaults)


def create_textpart(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return TextPart.objects.create(**defaults)


def create_timeclassification(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return TimeClassification.objects.create(**defaults)


def create_sourceofmetadatarecord(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return SourceOfMetadataRecord.objects.create(**defaults)


def create_subject(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return Subject.objects.create(**defaults)


def create_textclassification(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return TextClassification.objects.create(**defaults)


def create_textgenre(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return TextGenre.objects.create(**defaults)


def create_texttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return TextType.objects.create(**defaults)


def create_version(**kwargs):
    defaults = {}
    defaults["digest"] = "digest"
    defaults["date"] = "date"
    defaults.update(**kwargs)
    return Version.objects.create(**defaults)


class ResourceMetadataRecordViewTest(unittest.TestCase):
    """
    Tests for ResourceMetadataRecord
    """

    def setUp(self):
        self.client = Client()

    def test_list_resourcemetadatarecord(self):
        url = reverse('repository_resourcemetadatarecord_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_resourcemetadatarecord(self):
        url = reverse('repository_resourcemetadatarecord_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_resourcemetadatarecord(self):
        resourcemetadatarecord = create_resourcemetadatarecord()
        url = reverse('repository_resourcemetadatarecord_detail', args=[resourcemetadatarecord.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_resourcemetadatarecord(self):
        resourcemetadatarecord = create_resourcemetadatarecord()
        data = {
        }
        url = reverse('repository_resourcemetadatarecord_update', args=[resourcemetadatarecord.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ActorViewTest(unittest.TestCase):
    """
    Tests for Actor
    """

    def setUp(self):
        self.client = Client()

    def test_list_actor(self):
        url = reverse('repository_actor_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_actor(self):
        url = reverse('repository_actor_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_actor(self):
        actor = create_actor()
        url = reverse('repository_actor_detail', args=[actor.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_actor(self):
        actor = create_actor()
        data = {
        }
        url = reverse('repository_actor_update', args=[actor.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class AnnotationViewTest(unittest.TestCase):
    """
    Tests for Annotation
    """

    def setUp(self):
        self.client = Client()

    def test_list_annotation(self):
        url = reverse('repository_annotation_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_annotation(self):
        url = reverse('repository_annotation_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_annotation(self):
        annotation = create_annotation()
        url = reverse('repository_annotation_detail', args=[annotation.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_annotation(self):
        annotation = create_annotation()
        data = {
        }
        url = reverse('repository_annotation_update', args=[annotation.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class AnnotationTypeViewTest(unittest.TestCase):
    """
    Tests for AnnotationType
    """

    def setUp(self):
        self.client = Client()

    def test_list_annotationtype(self):
        url = reverse('repository_annotationtype_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_annotationtype(self):
        url = reverse('repository_annotationtype_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_annotationtype(self):
        annotationtype = create_annotationtype()
        url = reverse('repository_annotationtype_detail', args=[annotationtype.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_annotationtype(self):
        annotationtype = create_annotationtype()
        data = {
        }
        url = reverse('repository_annotationtype_update', args=[annotationtype.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CharacterEncodingViewTest(unittest.TestCase):
    """
    Tests for CharacterEncoding
    """

    def setUp(self):
        self.client = Client()

    def test_list_characterencoding(self):
        url = reverse('repository_characterencoding_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_characterencoding(self):
        url = reverse('repository_characterencoding_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_characterencoding(self):
        characterencoding = create_characterencoding()
        url = reverse('repository_characterencoding_detail', args=[characterencoding.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_characterencoding(self):
        characterencoding = create_characterencoding()
        data = {
        }
        url = reverse('repository_characterencoding_update', args=[characterencoding.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CommonInfoViewTest(unittest.TestCase):
    """
    Tests for CommonInfo
    """

    def setUp(self):
        self.client = Client()

    def test_list_commoninfo(self):
        url = reverse('repository_commoninfo_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_commoninfo(self):
        url = reverse('repository_commoninfo_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_commoninfo(self):
        commoninfo = create_commoninfo()
        url = reverse('repository_commoninfo_detail', args=[commoninfo.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_commoninfo(self):
        commoninfo = create_commoninfo()
        data = {
        }
        url = reverse('repository_commoninfo_update', args=[commoninfo.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ComponentDependencyViewTest(unittest.TestCase):
    """
    Tests for ComponentDependency
    """

    def setUp(self):
        self.client = Client()

    def test_list_componentdependency(self):
        url = reverse('repository_componentdependency_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_componentdependency(self):
        url = reverse('repository_componentdependency_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_componentdependency(self):
        componentdependency = create_componentdependency()
        url = reverse('repository_componentdependency_detail', args=[componentdependency.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_componentdependency(self):
        componentdependency = create_componentdependency()
        data = {
        }
        url = reverse('repository_componentdependency_update', args=[componentdependency.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ComponentDistributionViewTest(unittest.TestCase):
    """
    Tests for ComponentDistribution
    """

    def setUp(self):
        self.client = Client()

    def test_list_componentdistribution(self):
        url = reverse('repository_componentdistribution_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_componentdistribution(self):
        url = reverse('repository_componentdistribution_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_componentdistribution(self):
        componentdistribution = create_componentdistribution()
        url = reverse('repository_componentdistribution_detail', args=[componentdistribution.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_componentdistribution(self):
        componentdistribution = create_componentdistribution(operating_system=['windows'])
        data = {
            'operating_system': 'windows'
        }
        url = reverse('repository_componentdistribution_update', args=[componentdistribution.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ContactViewTest(unittest.TestCase):
    """
    Tests for Contact
    """

    def setUp(self):
        self.client = Client()

    def test_list_contact(self):
        url = reverse('repository_contact_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_contact(self):
        url = reverse('repository_contact_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_contact(self):
        contact = create_contact()
        url = reverse('repository_contact_detail', args=[contact.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_contact(self):
        contact = create_contact()
        data = {
        }
        url = reverse('repository_contact_update', args=[contact.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LanguageResourceViewTest(unittest.TestCase):
    """
    Tests for LanguageResource
    """

    def setUp(self):
        self.client = Client()

    def test_list_languageresource(self):
        url = reverse('repository_languageresource_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_languageresource(self):
        url = reverse('repository_languageresource_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_languageresource(self):
        languageresource = create_languageresource()
        url = reverse('repository_languageresource_detail', args=[languageresource.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_languageresource(self):
        languageresource = create_languageresource()
        data = {
        }
        url = reverse('repository_languageresource_update', args=[languageresource.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class DatasetDistributionViewTest(unittest.TestCase):
    """
    Tests for DatasetDistribution
    """

    def setUp(self):
        self.client = Client()

    def test_list_datasetdistribution(self):
        url = reverse('repository_datasetdistribution_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_datasetdistribution(self):
        url = reverse('repository_datasetdistribution_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_datasetdistribution(self):
        datasetdistribution = create_datasetdistribution()
        url = reverse('repository_datasetdistribution_detail', args=[datasetdistribution.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_datasetdistribution(self):
        datasetdistribution = create_datasetdistribution()
        data = {
        }
        url = reverse('repository_datasetdistribution_update', args=[datasetdistribution.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class DateViewTest(unittest.TestCase):
    """
    Tests for Date
    """

    def setUp(self):
        self.client = Client()

    def test_list_date(self):
        url = reverse('repository_date_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_date(self):
        url = reverse('repository_date_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_date(self):
        date = create_date()
        url = reverse('repository_date_detail', args=[date.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_date(self):
        date = create_date()
        data = {
        }
        url = reverse('repository_date_update', args=[date.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class DateRangeTypeViewTest(unittest.TestCase):
    """
    Tests for DateRangeType
    """

    def setUp(self):
        self.client = Client()

    def test_list_daterangetype(self):
        url = reverse('repository_daterangetype_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_daterangetype(self):
        url = reverse('repository_daterangetype_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_daterangetype(self):
        daterangetype = create_daterangetype()
        url = reverse('repository_daterangetype_detail', args=[daterangetype.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_daterangetype(self):
        daterangetype = create_daterangetype()
        data = {
        }
        url = reverse('repository_daterangetype_update', args=[daterangetype.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class DateCombinationViewTest(unittest.TestCase):
    """
    Tests for DateCombination
    """

    def setUp(self):
        self.client = Client()

    def test_list_datecombination(self):
        url = reverse('repository_datecombination_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_datecombination(self):
        url = reverse('repository_datecombination_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_datecombination(self):
        datecombination = create_datecombination()
        url = reverse('repository_datecombination_detail', args=[datecombination.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_datecombination(self):
        datecombination = create_datecombination()
        data = {
        }
        url = reverse('repository_datecombination_update', args=[datecombination.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class DistributionRightsViewTest(unittest.TestCase):
    """
    Tests for DistributionRights
    """

    def setUp(self):
        self.client = Client()

    def test_list_distributionrights(self):
        url = reverse('repository_distributionrights_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_distributionrights(self):
        url = reverse('repository_distributionrights_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_distributionrights(self):
        distributionrights = create_distributionrights()
        url = reverse('repository_distributionrights_detail', args=[distributionrights.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_distributionrights(self):
        distributionrights = create_distributionrights()
        data = {
        }
        url = reverse('repository_distributionrights_update', args=[distributionrights.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CreationViewTest(unittest.TestCase):
    """
    Tests for Creation
    """

    def setUp(self):
        self.client = Client()

    def test_list_creation(self):
        url = reverse('repository_creation_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_creation(self):
        url = reverse('repository_creation_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_creation(self):
        creation = create_creation()
        url = reverse('repository_creation_detail', args=[creation.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_creation(self):
        creation = create_creation()
        data = {
        }
        url = reverse('repository_creation_update', args=[creation.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class DataFormatViewTest(unittest.TestCase):
    """
    Tests for DataFormat
    """

    def setUp(self):
        self.client = Client()

    def test_list_dataformat(self):
        url = reverse('repository_dataformat_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_dataformat(self):
        url = reverse('repository_dataformat_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_dataformat(self):
        dataformat = create_dataformat()
        url = reverse('repository_dataformat_detail', args=[dataformat.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_dataformat(self):
        dataformat = create_dataformat()
        data = {
        }
        url = reverse('repository_dataformat_update', args=[dataformat.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class DocumentViewTest(unittest.TestCase):
    """
    Tests for Document
    """

    def setUp(self):
        self.client = Client()

    def test_list_document(self):
        url = reverse('repository_document_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_document(self):
        url = reverse('repository_document_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_document(self):
        document = create_document()
        url = reverse('repository_document_detail', args=[document.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_document(self):
        document = create_document()
        data = {
        }
        url = reverse('repository_document_update', args=[document.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class FunctionInfoViewTest(unittest.TestCase):
    """
    Tests for FunctionInfo
    """

    def setUp(self):
        self.client = Client()

    def test_list_functioninfo(self):
        url = reverse('repository_functioninfo_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_functioninfo(self):
        url = reverse('repository_functioninfo_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_functioninfo(self):
        functioninfo = create_functioninfo()
        url = reverse('repository_functioninfo_detail', args=[functioninfo.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_functioninfo(self):
        functioninfo = create_functioninfo()
        data = {
        }
        url = reverse('repository_functioninfo_update', args=[functioninfo.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class GeographicClassificationViewTest(unittest.TestCase):
    """
    Tests for GeographicClassification
    """

    def setUp(self):
        self.client = Client()

    def test_list_geographicclassification(self):
        url = reverse('repository_geographicclassification_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_geographicclassification(self):
        url = reverse('repository_geographicclassification_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_geographicclassification(self):
        geographicclassification = create_geographicclassification()
        url = reverse('repository_geographicclassification_detail', args=[geographicclassification.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_geographicclassification(self):
        geographicclassification = create_geographicclassification()
        data = {
        }
        url = reverse('repository_geographicclassification_update', args=[geographicclassification.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class GroupIdentifierViewTest(unittest.TestCase):
    """
    Tests for GroupIdentifier
    """

    def setUp(self):
        self.client = Client()

    def test_list_groupidentifier(self):
        url = reverse('repository_groupidentifier_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_groupidentifier(self):
        url = reverse('repository_groupidentifier_create')
        data = {
            "value": "value",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_groupidentifier(self):
        groupidentifier = create_groupidentifier()
        url = reverse('repository_groupidentifier_detail', args=[groupidentifier.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_groupidentifier(self):
        groupidentifier = create_groupidentifier()
        data = {
            "value": "value",
        }
        url = reverse('repository_groupidentifier_update', args=[groupidentifier.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class IdentificationViewTest(unittest.TestCase):
    """
    Tests for Identification
    """

    def setUp(self):
        self.client = Client()

    def test_list_identification(self):
        url = reverse('repository_identification_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_identification(self):
        url = reverse('repository_identification_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_identification(self):
        identification = create_identification()
        url = reverse('repository_identification_detail', args=[identification.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_identification(self):
        identification = create_identification()
        data = {
        }
        url = reverse('repository_identification_update', args=[identification.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LanguageViewTest(unittest.TestCase):
    """
    Tests for Language
    """

    def setUp(self):
        self.client = Client()

    def test_list_language(self):
        url = reverse('repository_language_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_language(self):
        url = reverse('repository_language_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_language(self):
        language = create_language()
        url = reverse('repository_language_detail', args=[language.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_language(self):
        language = create_language()
        data = {
        }
        url = reverse('repository_language_update', args=[language.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LanguageInfoViewTest(unittest.TestCase):
    """
    Tests for LanguageInfo
    """

    def setUp(self):
        self.client = Client()

    def test_list_languageinfo(self):
        url = reverse('repository_languageinfo_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_languageinfo(self):
        url = reverse('repository_languageinfo_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_languageinfo(self):
        languageinfo = create_languageinfo()
        url = reverse('repository_languageinfo_detail', args=[languageinfo.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_languageinfo(self):
        languageinfo = create_languageinfo()
        data = {
        }
        url = reverse('repository_languageinfo_update', args=[languageinfo.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LanguageVarietyViewTest(unittest.TestCase):
    """
    Tests for LanguageVariety
    """

    def setUp(self):
        self.client = Client()

    def test_list_languagevariety(self):
        url = reverse('repository_languagevariety_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_languagevariety(self):
        url = reverse('repository_languagevariety_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_languagevariety(self):
        languagevariety = create_languagevariety()
        url = reverse('repository_languagevariety_detail', args=[languagevariety.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_languagevariety(self):
        languagevariety = create_languagevariety()
        data = {
        }
        url = reverse('repository_languagevariety_update', args=[languagevariety.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LicenceViewTest(unittest.TestCase):
    """
    Tests for Licence
    """

    def setUp(self):
        self.client = Client()

    def test_list_licence(self):
        url = reverse('repository_licence_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_licence(self):
        url = reverse('repository_licence_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_licence(self):
        licence = create_licence()
        url = reverse('repository_licence_detail', args=[licence.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_licence(self):
        licence = create_licence()
        data = {
        }
        url = reverse('repository_licence_update', args=[licence.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LicenceIdentifierViewTest(unittest.TestCase):
    """
    Tests for LicenceIdentifier
    """

    def setUp(self):
        self.client = Client()

    def test_list_licenceidentifier(self):
        url = reverse('repository_licenceidentifier_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_licenceidentifier(self):
        url = reverse('repository_licenceidentifier_create')
        data = {
            "value": "value",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_licenceidentifier(self):
        licenceidentifier = create_licenceidentifier()
        url = reverse('repository_licenceidentifier_detail', args=[licenceidentifier.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_licenceidentifier(self):
        licenceidentifier = create_licenceidentifier()
        data = {
            "value": "value",
        }
        url = reverse('repository_licenceidentifier_update', args=[licenceidentifier.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LingualityViewTest(unittest.TestCase):
    """
    Tests for Linguality
    """

    def setUp(self):
        self.client = Client()

    def test_list_linguality(self):
        url = reverse('repository_linguality_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_linguality(self):
        url = reverse('repository_linguality_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_linguality(self):
        linguality = create_linguality()
        url = reverse('repository_linguality_detail', args=[linguality.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_linguality(self):
        linguality = create_linguality()
        data = {
        }
        url = reverse('repository_linguality_update', args=[linguality.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class MailingListViewTest(unittest.TestCase):
    """
    Tests for MailingList
    """

    def setUp(self):
        self.client = Client()

    def test_list_mailinglist(self):
        url = reverse('repository_mailinglist_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_mailinglist(self):
        url = reverse('repository_mailinglist_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_mailinglist(self):
        mailinglist = create_mailinglist()
        url = reverse('repository_mailinglist_detail', args=[mailinglist.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_mailinglist(self):
        mailinglist = create_mailinglist()
        data = {
        }
        url = reverse('repository_mailinglist_update', args=[mailinglist.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class MediaPartViewTest(unittest.TestCase):
    """
    Tests for MediaPart
    """

    def setUp(self):
        self.client = Client()

    def test_list_mediapart(self):
        url = reverse('repository_mediapart_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_mediapart(self):
        url = reverse('repository_mediapart_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_mediapart(self):
        mediapart = create_mediapart()
        url = reverse('repository_mediapart_detail', args=[mediapart.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_mediapart(self):
        mediapart = create_mediapart()
        data = {
        }
        url = reverse('repository_mediapart_update', args=[mediapart.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class MediaTechnicalInfoViewTest(unittest.TestCase):
    """
    Tests for MediaTechnicalInfo
    """

    def setUp(self):
        self.client = Client()

    def test_list_mediatechnicalinfo(self):
        url = reverse('repository_mediatechnicalinfo_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_mediatechnicalinfo(self):
        url = reverse('repository_mediatechnicalinfo_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_mediatechnicalinfo(self):
        mediatechnicalinfo = create_mediatechnicalinfo()
        url = reverse('repository_mediatechnicalinfo_detail', args=[mediatechnicalinfo.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_mediatechnicalinfo(self):
        mediatechnicalinfo = create_mediatechnicalinfo()
        data = {
        }
        url = reverse('repository_mediatechnicalinfo_update', args=[mediatechnicalinfo.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class MetadataHeaderViewTest(unittest.TestCase):
    """
    Tests for MetadataHeader
    """

    def setUp(self):
        self.client = Client()

    def test_list_metadataheader(self):
        url = reverse('repository_metadataheader_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_metadataheader(self):
        url = reverse('repository_metadataheader_create')
        data = {
            "revision": "revision",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_metadataheader(self):
        metadataheader = create_metadataheader()
        url = reverse('repository_metadataheader_detail', args=[metadataheader.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_metadataheader(self):
        metadataheader = create_metadataheader()
        data = {
            "revision": "revision",
        }
        url = reverse('repository_metadataheader_update', args=[metadataheader.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class MetadataIdentifierViewTest(unittest.TestCase):
    """
    Tests for MetadataIdentifier
    """

    def setUp(self):
        self.client = Client()

    def test_list_metadataidentifier(self):
        url = reverse('repository_metadataidentifier_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_metadataidentifier(self):
        url = reverse('repository_metadataidentifier_create')
        data = {
            "value": "value",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_metadataidentifier(self):
        metadataidentifier = create_metadataidentifier()
        url = reverse('repository_metadataidentifier_detail', args=[metadataidentifier.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_metadataidentifier(self):
        metadataidentifier = create_metadataidentifier()
        data = {
            "value": "value",
        }
        url = reverse('repository_metadataidentifier_update', args=[metadataidentifier.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class OrganizationIdentifierViewTest(unittest.TestCase):
    """
    Tests for OrganizationIdentifier
    """

    def setUp(self):
        self.client = Client()

    def test_list_organizationidentifier(self):
        url = reverse('repository_organizationidentifier_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_organizationidentifier(self):
        url = reverse('repository_organizationidentifier_create')
        data = {
            "value": "value",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_organizationidentifier(self):
        organizationidentifier = create_organizationidentifier()
        url = reverse('repository_organizationidentifier_detail', args=[organizationidentifier.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_organizationidentifier(self):
        organizationidentifier = create_organizationidentifier()
        data = {
            "value": "value",
        }
        url = reverse('repository_organizationidentifier_update', args=[organizationidentifier.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class PersonIdentifierViewTest(unittest.TestCase):
    """
    Tests for PersonIdentifier
    """

    def setUp(self):
        self.client = Client()

    def test_list_personidentifier(self):
        url = reverse('repository_personidentifier_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_personidentifier(self):
        url = reverse('repository_personidentifier_create')
        data = {
            "value": "value",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_personidentifier(self):
        personidentifier = create_personidentifier()
        url = reverse('repository_personidentifier_detail', args=[personidentifier.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_personidentifier(self):
        personidentifier = create_personidentifier()
        data = {
            "value": "value",
        }
        url = reverse('repository_personidentifier_update', args=[personidentifier.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class PhysicalResourceViewTest(unittest.TestCase):
    """
    Tests for PhysicalResource
    """

    def setUp(self):
        self.client = Client()

    def test_list_physicalresource(self):
        url = reverse('repository_physicalresource_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_physicalresource(self):
        url = reverse('repository_physicalresource_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_physicalresource(self):
        physicalresource = create_physicalresource()
        url = reverse('repository_physicalresource_detail', args=[physicalresource.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_physicalresource(self):
        physicalresource = create_physicalresource()
        data = {
        }
        url = reverse('repository_physicalresource_update', args=[physicalresource.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ModalityViewTest(unittest.TestCase):
    """
    Tests for Modality
    """

    def setUp(self):
        self.client = Client()

    def test_list_modality(self):
        url = reverse('repository_modality_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_modality(self):
        url = reverse('repository_modality_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_modality(self):
        modality = create_modality()
        url = reverse('repository_modality_detail', args=[modality.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_modality(self):
        modality = create_modality()
        data = {
        }
        url = reverse('repository_modality_update', args=[modality.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProcessingResourceViewTest(unittest.TestCase):
    """
    Tests for ProcessingResource
    """

    def setUp(self):
        self.client = Client()

    def test_list_processingresource(self):
        url = reverse('repository_processingresource_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_processingresource(self):
        url = reverse('repository_processingresource_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_processingresource(self):
        processingresource = create_processingresource()
        url = reverse('repository_processingresource_detail', args=[processingresource.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_processingresource(self):
        processingresource = create_processingresource()
        data = {
        }
        url = reverse('repository_processingresource_update', args=[processingresource.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class PublicationIdentifierViewTest(unittest.TestCase):
    """
    Tests for PublicationIdentifier
    """

    def setUp(self):
        self.client = Client()

    def test_list_publicationidentifier(self):
        url = reverse('repository_publicationidentifier_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_publicationidentifier(self):
        url = reverse('repository_publicationidentifier_create')
        data = {
            "value": "value",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_publicationidentifier(self):
        publicationidentifier = create_publicationidentifier()
        url = reverse('repository_publicationidentifier_detail', args=[publicationidentifier.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_publicationidentifier(self):
        publicationidentifier = create_publicationidentifier()
        data = {
            "value": "value",
        }
        url = reverse('repository_publicationidentifier_update', args=[publicationidentifier.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class RegisterViewTest(unittest.TestCase):
    """
    Tests for Register
    """

    def setUp(self):
        self.client = Client()

    def test_list_register(self):
        url = reverse('repository_register_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_register(self):
        url = reverse('repository_register_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_register(self):
        register = create_register()
        url = reverse('repository_register_detail', args=[register.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_register(self):
        register = create_register()
        data = {
        }
        url = reverse('repository_register_update', args=[register.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class RepositoryViewTest(unittest.TestCase):
    """
    Tests for Repository
    """

    def setUp(self):
        self.client = Client()

    def test_list_repository(self):
        url = reverse('repository_repository_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_repository(self):
        url = reverse('repository_repository_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_repository(self):
        repository = create_repository()
        url = reverse('repository_repository_detail', args=[repository.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_repository(self):
        repository = create_repository()
        data = {
        }
        url = reverse('repository_repository_update', args=[repository.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class RepositoryIdentifierViewTest(unittest.TestCase):
    """
    Tests for RepositoryIdentifier
    """

    def setUp(self):
        self.client = Client()

    def test_list_repositoryidentifier(self):
        url = reverse('repository_repositoryidentifier_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_repositoryidentifier(self):
        url = reverse('repository_repositoryidentifier_create')
        data = {
            "value": "value",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_repositoryidentifier(self):
        repositoryidentifier = create_repositoryidentifier()
        url = reverse('repository_repositoryidentifier_detail', args=[repositoryidentifier.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_repositoryidentifier(self):
        repositoryidentifier = create_repositoryidentifier()
        data = {
            "value": "value",
        }
        url = reverse('repository_repositoryidentifier_update', args=[repositoryidentifier.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ResourceViewTest(unittest.TestCase):
    """
    Tests for Resource
    """

    def setUp(self):
        self.client = Client()

    def test_list_resource(self):
        url = reverse('repository_resource_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_resource(self):
        url = reverse('repository_resource_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_resource(self):
        resource = create_resource()
        url = reverse('repository_resource_detail', args=[resource.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_resource(self):
        resource = create_resource()
        data = {
        }
        url = reverse('repository_resource_update', args=[resource.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ResourceIdentifierViewTest(unittest.TestCase):
    """
    Tests for ResourceIdentifier
    """

    def setUp(self):
        self.client = Client()

    def test_list_resourceidentifier(self):
        url = reverse('repository_resourceidentifier_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_resourceidentifier(self):
        url = reverse('repository_resourceidentifier_create')
        data = {
            "value": "value",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_resourceidentifier(self):
        resourceidentifier = create_resourceidentifier()
        url = reverse('repository_resourceidentifier_detail', args=[resourceidentifier.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_resourceidentifier(self):
        resourceidentifier = create_resourceidentifier()
        data = {
            "value": "value",
        }
        url = reverse('repository_resourceidentifier_update', args=[resourceidentifier.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ResourceRightsViewTest(unittest.TestCase):
    """
    Tests for ResourceRights
    """

    def setUp(self):
        self.client = Client()

    def test_list_resourcerights(self):
        url = reverse('repository_resourcerights_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_resourcerights(self):
        url = reverse('repository_resourcerights_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_resourcerights(self):
        resourcerights = create_resourcerights()
        url = reverse('repository_resourcerights_detail', args=[resourcerights.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_resourcerights(self):
        resourcerights = create_resourcerights()
        data = {
        }
        url = reverse('repository_resourcerights_update', args=[resourcerights.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class SizeViewTest(unittest.TestCase):
    """
    Tests for Size
    """

    def setUp(self):
        self.client = Client()

    def test_list_size(self):
        url = reverse('repository_size_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_size(self):
        url = reverse('repository_size_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_size(self):
        size = create_size()
        url = reverse('repository_size_detail', args=[size.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_size(self):
        size = create_size()
        data = {
        }
        url = reverse('repository_size_update', args=[size.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TextFormatViewTest(unittest.TestCase):
    """
    Tests for TextFormat
    """

    def setUp(self):
        self.client = Client()

    def test_list_textformat(self):
        url = reverse('repository_textformat_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_textformat(self):
        url = reverse('repository_textformat_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_textformat(self):
        textformat = create_textformat()
        url = reverse('repository_textformat_detail', args=[textformat.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_textformat(self):
        textformat = create_textformat()
        data = {
        }
        url = reverse('repository_textformat_update', args=[textformat.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TextPartViewTest(unittest.TestCase):
    """
    Tests for TextPart
    """

    def setUp(self):
        self.client = Client()

    def test_list_textpart(self):
        url = reverse('repository_textpart_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_textpart(self):
        url = reverse('repository_textpart_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_textpart(self):
        textpart = create_textpart()
        url = reverse('repository_textpart_detail', args=[textpart.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_textpart(self):
        textpart = create_textpart()
        data = {
        }
        url = reverse('repository_textpart_update', args=[textpart.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TimeClassificationViewTest(unittest.TestCase):
    """
    Tests for TimeClassification
    """

    def setUp(self):
        self.client = Client()

    def test_list_timeclassification(self):
        url = reverse('repository_timeclassification_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_timeclassification(self):
        url = reverse('repository_timeclassification_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_timeclassification(self):
        timeclassification = create_timeclassification()
        url = reverse('repository_timeclassification_detail', args=[timeclassification.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_timeclassification(self):
        timeclassification = create_timeclassification()
        data = {
        }
        url = reverse('repository_timeclassification_update', args=[timeclassification.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class SourceOfMetadataRecordViewTest(unittest.TestCase):
    """
    Tests for SourceOfMetadataRecord
    """

    def setUp(self):
        self.client = Client()

    def test_list_sourceofmetadatarecord(self):
        url = reverse('repository_sourceofmetadatarecord_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_sourceofmetadatarecord(self):
        url = reverse('repository_sourceofmetadatarecord_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_sourceofmetadatarecord(self):
        sourceofmetadatarecord = create_sourceofmetadatarecord()
        url = reverse('repository_sourceofmetadatarecord_detail', args=[sourceofmetadatarecord.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_sourceofmetadatarecord(self):
        sourceofmetadatarecord = create_sourceofmetadatarecord()
        data = {
        }
        url = reverse('repository_sourceofmetadatarecord_update', args=[sourceofmetadatarecord.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class SubjectViewTest(unittest.TestCase):
    """
    Tests for Subject
    """

    def setUp(self):
        self.client = Client()

    def test_list_subject(self):
        url = reverse('repository_subject_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_subject(self):
        url = reverse('repository_subject_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_subject(self):
        subject = create_subject()
        url = reverse('repository_subject_detail', args=[subject.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_subject(self):
        subject = create_subject()
        data = {
        }
        url = reverse('repository_subject_update', args=[subject.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TextClassificationViewTest(unittest.TestCase):
    """
    Tests for TextClassification
    """

    def setUp(self):
        self.client = Client()

    def test_list_textclassification(self):
        url = reverse('repository_textclassification_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_textclassification(self):
        url = reverse('repository_textclassification_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_textclassification(self):
        textclassification = create_textclassification()
        url = reverse('repository_textclassification_detail', args=[textclassification.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_textclassification(self):
        textclassification = create_textclassification()
        data = {
        }
        url = reverse('repository_textclassification_update', args=[textclassification.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TextGenreViewTest(unittest.TestCase):
    """
    Tests for TextGenre
    """

    def setUp(self):
        self.client = Client()

    def test_list_textgenre(self):
        url = reverse('repository_textgenre_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_textgenre(self):
        url = reverse('repository_textgenre_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_textgenre(self):
        textgenre = create_textgenre()
        url = reverse('repository_textgenre_detail', args=[textgenre.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_textgenre(self):
        textgenre = create_textgenre()
        data = {
        }
        url = reverse('repository_textgenre_update', args=[textgenre.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TextTypeViewTest(unittest.TestCase):
    """
    Tests for TextType
    """

    def setUp(self):
        self.client = Client()

    def test_list_texttype(self):
        url = reverse('repository_texttype_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_texttype(self):
        url = reverse('repository_texttype_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_texttype(self):
        texttype = create_texttype()
        url = reverse('repository_texttype_detail', args=[texttype.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_texttype(self):
        texttype = create_texttype()
        data = {
        }
        url = reverse('repository_texttype_update', args=[texttype.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class VersionViewTest(unittest.TestCase):
    """
    Tests for Version
    """

    def setUp(self):
        self.client = Client()

    def test_list_version(self):
        url = reverse('repository_version_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_version(self):
        url = reverse('repository_version_create')
        data = {
            "digest": "digest",
            "date": "date",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_version(self):
        version = create_version()
        url = reverse('repository_version_detail', args=[version.pk, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_version(self):
        version = create_version()
        data = {
            "digest": "digest",
            "date": "date",
        }
        url = reverse('repository_version_update', args=[version.pk, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
