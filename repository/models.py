from django.contrib.postgres.fields import ArrayField, JSONField
from django.core import validators as dj_validators
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
# Create your models here.
from polymorphic.models import PolymorphicModel
from rest_framework.exceptions import ValidationError

from elg_platform import utils
from repository import enums
from repository import validators


class ResourceMetadataRecord(models.Model):
    # related fields
    metadata_header = models.OneToOneField(
        'repository.MetadataHeader',
        on_delete=models.CASCADE,
        related_name='resource_metadata_record',
        help_text='Groups information on the metadata record itself'
    )
    physical_resources = models.OneToOneField(
        'repository.PhysicalResource',
        on_delete=models.CASCADE,
        related_name='resource_metadata_record',
        help_text='',
        null=True, blank=True
    )

    common_info = models.OneToOneField(
        'repository.CommonInfo',
        on_delete=models.CASCADE,
        related_name='resource_metadata_record',
        help_text='',
        null=True, blank=True
    )

    language_resource = models.OneToOneField(
        'repository.LanguageResource',
        help_text='',
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return f'{utils.retrieve_best_lang(self.common_info.identification.resource_name)}'


class Actor(PolymorphicModel):
    """

    """

    class Meta:
        ordering = ('-pk',)

    type = models.CharField(
        help_text='The type of Actor',
        verbose_name='Actor Type',
        max_length=20,
        choices=enums.ACTOR_TYPE_CHOICES,
        editable=False
    )

    def as_subclass(self):
        # pylint: disable-msg=E1101
        subclasses = self.__class__.__subclasses__()
        for subclass in subclasses:
            subclass_name = subclass.__name__
            candidate_fieldname = subclass_name.lower()
            if hasattr(self, candidate_fieldname):
                childinstance = getattr(self, candidate_fieldname)
                return childinstance.as_subclass()  # recursive
        return self


class Annotation(models.Model):
    """
    Groups information on the annotated part(s) of a resource
    """

    class Meta:
        ordering = ('-pk',)

    # FK annotation_types --> AnnotationType

    type_system = models.ForeignKey(
        'repository.Resource',
        related_name='annotation_type_system',
        help_text='A name or an identifier (e.g. url reference) to the typesystem used in the annotation of the '
                  'resource or used by the component',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    annotation_schema = models.OneToOneField(
        'repository.Resource',
        related_name='annotation_annotation_schema',
        help_text='A set of elements and values designed to annotate data',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    annotation_resource = models.ForeignKey(
        'repository.Resource',
        related_name='annotation_annotation_resource',
        help_text='A name or an identifier (e.g. url reference) to the ancillary resource (e.g. tagset, ontology, '
                  'term list) used in the annotation of the resource or used by the component',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    # FK guidelines_documented_in --> Document

    annotation_mode = models.CharField(
        choices=enums.MODE,
        max_length=utils.get_max_length(enums.MODE),
        help_text='Indicates whether the resource is annotated manually or by automatic processes',
        null=True,
        blank=True
    )

    annotation_mode_details = models.CharField(
        max_length=500,
        help_text='Provides further information on annotation process ',
        null=True,
        blank=True
    )

    is_annotated_by = models.ManyToManyField(
        'repository.Resource',
        help_text='The name, the identifier or the url of the tool used for the annotation of the resource',
    )

    annotation_date = models.OneToOneField(
        'repository.DateCombination',
        help_text='The dates (either date or range of dates) in which the annotation process has taken place',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    interannotator_agreement = models.CharField(
        max_length=1000,
        help_text='Provides information on the interannotator agreement and the methods/metrics applied',
        null=True,
        blank=True
    )

    intraannotator_agreement = models.CharField(
        max_length=1000,
        help_text='Provides information on the intra-annotator agreement and the methods/metrics applied',
        null=True,
        blank=True
    )

    annotators = models.ManyToManyField(
        'repository.Actor',
        help_text='Groups information on the annotators of the specific annotation type'
    )

    size = models.OneToOneField(
        'repository.Size',
        help_text='Provides information on the size of the resource parts with different character encoding',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    text_part = models.ForeignKey(
        'repository.TextPart',
        related_name='annotations',
        help_text='Groups information on the annotated part(s) of a resource',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )


class AnnotationType(models.Model):
    """

    """

    class Meta:
        ordering = ('-pk',)

    annotation_type = models.URLField(
        choices=enums.ANNOTATION_TYPE,
        max_length=utils.get_max_length(enums.ANNOTATION_TYPE),
        help_text='Specifies the annotation type of the resource or what a s/w component consumes or produces '
                  'as an output'
    )

    annotation_type_other = models.CharField(
        max_length=100,
        help_text='',
        null=True,
        blank=True
    )

    annotation = models.ForeignKey(
        'repository.Annotation',
        related_name='annotation_types',
        help_text='',
        null=True,
        blank=True,
        on_delete=models.CASCADE
    )

    processing_resource = models.ForeignKey(
        'repository.ProcessingResource',
        related_name='annotation_types',
        help_text='',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )


# class AudioDatasetTechnicalInfo TODO

class CharacterEncoding(models.Model):
    """
    Groups together information on character encoding of the resource
    """

    class Meta:
        ordering = ('-pk',)

    encoding = models.CharField(
        help_text='The name of the character encoding used in the resource or supported by the component',
        choices=enums.ENCODING,
        max_length=utils.get_max_length(enums.ENCODING)
    )

    size = models.OneToOneField(
        'repository.Size',
        help_text='Provides information on the size of the resource parts with different character encoding',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    # FK TextDatasetTechnicalInfo
    text_dataset_technical_info = models.ForeignKey(
        'repository.TextDatasetTechnicalInfo',
        help_text='Groups information on the text format(s) of a resource',
        related_name='character_encodings',
        null=True, blank=True,
        on_delete=models.SET_NULL,
    )


class CommonInfo(models.Model):
    # related fields
    identification = models.OneToOneField(
        'repository.Identification',
        on_delete=models.CASCADE,
        related_name="common_info",
        help_text='Groups together information needed to identify and describe at a minimal level the resource'
    )
    version = models.OneToOneField(
        'repository.Version',
        on_delete=models.CASCADE,
        related_name="common_info",
        help_text='Groups information on a specific version or release of the resource'
    )

    contact = models.OneToOneField(
        'repository.Contact',
        on_delete=models.CASCADE,
        related_name="common_info",
        help_text='Groups information on the possible sources for obtaining further information regarding the '
                  'resource; at least a general email address or a landing page (website) must be supplied, '
                  'although the recommended practice is to describe a contact person or group'
    )

    resource_rights = models.OneToOneField(
        'repository.ResourceRights',
        on_delete=models.CASCADE,
        related_name="common_info",
        help_text='Groups information on copyright for a resource',
    )

    # FK documentations --> Documentation

    domains = models.ManyToManyField(
        'repository.Domain',
        related_name='common_info'
    )

    keywords = ArrayField(
        models.CharField(
            max_length=20
        ),
        help_text='A list words or phrases considered important for the classification of a resource and included '
                  'as tag in its metadata',
        null=True,
        blank=True
    )

    # FK relations --> Relation

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return u'%s' % self.pk


class ComponentDependency(models.Model):
    """
    Groups together information on resources required for the operation of a component (e.g. typesystem,
    annotation resources etc.)
    """

    class Meta:
        ordering = ('-pk',)

    type_system = models.ForeignKey(
        'repository.Resource',
        related_name='dependency_type_system',
        help_text='A name or an identifier (e.g. url reference) to the typesystem used in the annotation '
                  'of the resource or used by the component',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    annotation_schema = models.ForeignKey(
        'repository.Resource',
        related_name='dependency_annotation_schema',
        help_text='A set of elements and values designed to annotate data',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    ml_model = models.ForeignKey(
        'repository.Resource',
        related_name='dependency_ml_model',
        help_text='',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    # FK annotation_resources --> Resource

    required_software = ArrayField(
        models.CharField(
            max_length=100
        ),
        null=True,
        blank=True
    )

    tool_service = models.ForeignKey(
        'repository.ToolService',
        related_name='dependencies',
        help_text='Groups together information on resources required for the operation of a component '
                  '(e.g. typesystem, annotation resources etc.)',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )


class ComponentDistribution(models.Model):
    """
    Groups information on the distributable form of a component (e.g. as a software code, an executable
    programme, a web service etc.)
    """

    class Meta:
        ordering = ('-pk',)

    distribution_form = models.CharField(
        choices=enums.COMPONENT_DIST_FORM,
        max_length=utils.get_max_length(enums.COMPONENT_DIST_FORM),
        help_text='Specifies the form of distribution of the component (e.g. as source code, executable programme '
                  'etc.)',
    )

    distribution_location = models.URLField(
        help_text='Any location (e.g. URL, repository, etc.) where the resource can be accessed from, either in '
                  'a downloadable form or as a web executable command etc.'
    )

    command = models.CharField(
        max_length=100,
        help_text='A command line used to invoke a component',
        null=True,
        blank=True
    )

    web_service_type = models.CharField(
        choices=enums.WEB_SERVICE_TYPE,
        max_length=utils.get_max_length(enums.WEB_SERVICE_TYPE),
        help_text='A classification of web services based on the protocol used for accessing them',
        null=True,
        blank=True
    )

    operating_system = ArrayField(
        models.CharField(
            choices=enums.OPERATING_SYSTEM,
            max_length=utils.get_max_length(enums.OPERATING_SYSTEM),
            help_text='The operating system on which the tool can run'
        ),
        help_text='The operating systems on which the tool can run'
    )

    distribution_rights = models.OneToOneField(
        'repository.DistributionRights',
        help_text='Groups information on licensing terms for the distribution of a resource; at least a licence '
                  '(preferred option) or formal rights statement (e.g. open access) must be declared for the resource',
        on_delete=models.CASCADE
    )

    tool_service = models.ForeignKey(
        'repository.ToolService',
        related_name='distributions',
        null=True,
        blank=True,
        on_delete=models.PROTECT
    )


class Contact(models.Model):
    """

    """

    class Meta:
        ordering = ('-pk',)

    contact_point = models.CharField(
        max_length=200,
        help_text='An email or a URL (landing page) that provides more information on the resource',
    )
    type = models.CharField(
        max_length=utils.get_max_length(enums.CONTACT_TYPE),
        choices=enums.CONTACT_TYPE,
        help_text='The type of the contact point (email or URL)'
    )

    contact_persons = models.ManyToManyField(
        'repository.Person',
        related_name="contacts",
        help_text='Groups information on the person(s) that is/are responsible for providing further information '
                  'regarding the resource',
    )
    contact_organizations = models.ManyToManyField(
        'repository.Organization',
        related_name="contacts",
        help_text='Groups information on the organization(s) that is/are responsible for providing further '
                  'information regarding the resource',
    )
    contact_groups = models.ManyToManyField(
        'repository.Group',
        related_name="contacts",
        help_text='Groups information on the group(s) that is/are responsible for providing further information '
                  'regarding the resource',
    )
    mailing_lists = models.ManyToManyField(
        'repository.MailingList',
        related_name="contacts",
        help_text='Groups information on the mailing list(s) that can be used for further information '
                  'regarding the resource',
    )

    def check_info_integrity(self):
        # Check if the contact_point value is valid, given the type specified
        if self.type == 'landingPage':
            validators.validate_url(self.contact_point)
        elif self.type == 'contactEmail':
            validators.validate_email(self.contact_point)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.check_info_integrity()
        super().save()

    def __str__(self):
        return self.contact_point


class LanguageResource(PolymorphicModel):
    """

    """

    class Meta:
        ordering = ('-pk',)

    resource_type = models.CharField(
        max_length=utils.get_max_length(enums.RESOURCE_TYPE),
        choices=enums.RESOURCE_TYPE,
        help_text='The type of the resource',
        editable=False
    )

    def as_subclass(self):
        # pylint: disable-msg=E1101
        subclasses = self.__class__.__subclasses__()
        for subclass in subclasses:
            subclass_name = subclass.__name__
            candidate_fieldname = subclass_name.lower()
            if hasattr(self, candidate_fieldname):
                childinstance = getattr(self, candidate_fieldname)
                return childinstance.as_subclass()  # recursive
        return self


class Corpus(LanguageResource):
    class Meta:
        ordering = ('-pk',)

    dataset_distribution = models.OneToOneField(
        'repository.DatasetDistribution',
        help_text='Groups information on the distribution of a dataset resource (multiple files)',
        on_delete=models.CASCADE
    )

    linguality = models.OneToOneField(
        'repository.Linguality',
        help_text='Groups information on the number of languages of the resource part and of the way '
                  'they are combined to each other',
        on_delete=models.CASCADE
    )

    # FK languages -> LanguageInfo
    # FK time_classifications -> TimeClassification
    # FK geographic_classifications -> GeographicClassification

    user_query = models.CharField(
        max_length=500,
        help_text='The query text that has created the corpus of scholarly publications',
        null=True,
        blank=True
    )

    # FK corpus_parts --> MediaPart

    def save(self, *args, **kwargs):
        self.resource_type = 'corpus'
        super().save()


class DatasetDistribution(models.Model):
    """
    Groups information on the distribution of a dataset resource (multiple files)
    """

    class Meta:
        ordering = ('-pk',)

    distribution_medium = models.CharField(
        max_length=utils.get_max_length(enums.DISTRIBUTION_MEDIUM),
        choices=enums.DISTRIBUTION_MEDIUM,
        help_text='Specifies the medium (channel) used for delivering or providing access to the resource'
    )

    distribution_location = models.URLField(
        help_text='Any location where the resource can be downloaded from',
        null=True,
        blank=True
    )

    distribution_rights = models.OneToOneField(
        'repository.DistributionRights',
        help_text='Groups information on licensing terms for the distribution of a resource; at least a '
                  'licence (preferred option) or formal rights statement (e.g. open access) must be '
                  'declared for the resource',
        on_delete=models.CASCADE
    )

    # FK technical_parts --> MediaTechnicalInfo


class Date(models.Model):
    """
    Base type to be used for dates, allowing optional day and month
    """

    class Meta:
        ordering = ('-pk',)

    day = models.PositiveSmallIntegerField(
        help_text='Optional day, always in two-digit format',
        validators=[MinValueValidator(1), MaxValueValidator(31)],
        null=True,
        blank=True
    )

    month = models.PositiveSmallIntegerField(
        help_text='Optional month; to be used in two-digit format',
        validators=[MinValueValidator(1), MaxValueValidator(12)],
        null=True,
        blank=True
    )

    year = models.PositiveSmallIntegerField(
        help_text='for year; to be used with four digits always',
        validators=[MinValueValidator(1400), MaxValueValidator(2100)],
        null=True,
        blank=True
    )


class DateRangeType(models.Model):
    # Add 'Type' to name to prevent clash with psycopg2._range.DateRange
    """
    Base type to be used for range of dates (time periods)
    """

    class Meta:
        ordering = ('-pk',)

    start_date = models.OneToOneField(
        'repository.Date',
        related_name='start_date',
        help_text='Basic format for start dates, with optional day and month and obligatory year',
        on_delete=models.CASCADE
    )

    end_date = models.OneToOneField(
        'repository.Date',
        related_name='end_date',
        help_text='Basic format for end dates, with optional day and month and obligatory year',
        on_delete=models.CASCADE
    )


class DateCombination(models.Model):
    """
    Base type to be used for choice between range of dates and exact date
    """

    class Meta:
        ordering = ('-pk',)

    date = models.OneToOneField(
        'repository.Date',
        help_text='Basic format for dates, with optional day and month and obligatory year',
        null=True,
        blank=True,
        on_delete=models.CASCADE
    )

    date_range = models.OneToOneField(
        'repository.DateRangeType',  # Add 'Type' to name to prevent clash with psycopg2._range.DateRange
        help_text='Basic format for date ranges, allowing combinations of years and full dates',
        null=True,
        blank=True,
        on_delete=models.CASCADE
    )

    def save(self, *args, **kwargs):
        # check for mutual exclusion
        if self.date and self.date_range:
            raise ValidationError("You can define either name or identifiers")
        elif not self.date and not self.date_range:
            raise ValidationError("You must define either name or identifiers")
        super().save()


class DistributionRights(models.Model):
    """
    Groups information on licensing terms for the distribution of a resource; at least a licence
    (preferred option) or formal rights statement (e.g. open access) must be declared for the resource
    """

    class Meta:
        ordering = ('-pk',)

    licences = models.ManyToManyField(
        'repository.Licence',
        help_text='Base type for related licences; choice between project names and identifiers',
    )

    rights_statement = models.CharField(
        help_text='The name (identifier) of a formal statement indicative of licensing terms for the use of a '
                  'resource (e.g. open access, free to read etc.); its semantics should be clear, preferrably '
                  'formally expressed and stored at a URL',
        max_length=utils.get_max_length(enums.RIGHTS_STATEMENT),
        choices=enums.RIGHTS_STATEMENT
    )

    attribution_text = models.CharField(
        max_length=1500,
        help_text='The text that must be quoted for attribution purposes when using a resource - for cases where '
                  'a resource is provided with a restriction on attribution; you can use a standard text such as '
                  '"Resource A by Resource Creator/Owner B used under licence C as accessed at D"',
        null=True,
        blank=True
    )

    availability_start_date = models.DateField(
        help_text='Specifies the start date of availability of a resource - only for cases where a resource is '
                  'available for a restricted time period.',
        null=True,
        blank=True
    )

    availability_end_date = models.DateField(
        help_text='Specifies the end date of availability of a resource - only for cases where a resource is '
                  'available for a restricted time period.',
        null=True,
        blank=True
    )


class Creation(models.Model):
    """
    Groups together information on the resource creation (e.g. for corpora, selection of texts/audio files/
    video files etc.; for lexica, construction of lemma list etc.)
    """

    class Meta:
        ordering = ('-pk',)

    original_sources = models.ManyToManyField(
        'repository.Resource',
        help_text='',
        related_name='original_sources'
    )

    creation_mode = models.CharField(
        choices=enums.MODE,
        max_length=utils.get_max_length(enums.MODE),
        help_text='Specifies whether the resource is created automatically or in a manual or interactive mode',
        null=True,
        blank=True
    )

    creation_mode_details = models.CharField(
        max_length=500,
        help_text='Provides further information on the creation methods and processes',
        null=True,
        blank=True
    )

    created_by = models.ManyToManyField(
        'repository.Resource',
        help_text='',
        related_name='created_by'
    )


class DataFormat(models.Model):
    """
    Specifies the format that is used since often the mime type will not be sufficient for machine processing;
    NOTE: normally the format should be represented as a combination of the mimetype (e.g. application/xml) and
    some name and link to the documentation about the supplementary conventions used (e.g xces, alvisED etc.)
    """

    class Meta:
        ordering = ('-pk',)

    data_format = models.URLField(
        choices=enums.DATA_FORMAT,
        max_length=utils.get_max_length(enums.DATA_FORMAT),
        help_text=''
    )

    data_format_other = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        help_text=''
    )

    processing_resource = models.ForeignKey(
        'repository.ProcessingResource',
        related_name='data_formats',
        help_text='Specifies the format that is used since often the mime type will not be sufficient for '
                  'machine processing; NOTE: normally the format should be represented as a combination '
                  'of the mimetype (e.g. application/xml) and some name and link to the documentation '
                  'about the supplementary conventions used (e.g xces, alvisED etc.)',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )


class Document(models.Model):
    """

    """

    class Meta:
        ordering = ('-pk',)

    document_unstructured = models.CharField(
        max_length=500,
        help_text='Used to encode a bibliographic record, preferrably in the form of a bibtex record or as a free '
                  'text description'
    )

    publication_identifiers = models.ManyToManyField(
        'repository.PublicationIdentifier',
        related_name="documents",
        help_text='Reference to a DOI (recommended) or any kind of identifier used for the publication',
    )

    annotation = models.ForeignKey(
        'repository.Annotation',
        related_name='guidelines_documented_in',
        help_text='A bibliographic reference or ms:httpURI link to the annotation manual',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )


class Documentation(models.Model):
    """

    """

    class Meta:
        ordering = ('-pk',)

    documentation_type = models.CharField(
        choices=enums.DOCUMENTATION_TYPE,
        max_length=utils.get_max_length(enums.DOCUMENTATION_TYPE),
        help_text=''
    )

    document_bibtex = models.CharField(
        max_length=200,
        help_text='',
        null=True, blank=True
    )

    documentation_bibliographic_data = models.CharField(
        max_length=200,
        help_text='',
        null=True, blank=True
    )

    publication_identifiers = models.ManyToManyField(
        'repository.PublicationIdentifier',
        related_name="documentations",
        help_text='Reference to a DOI (recommended) or any kind of identifier used for the publication',
    )

    common_info = models.ForeignKey(
        'repository.CommonInfo',
        related_name='documentations',
        help_text='Groups together information on any document (publication, report, manual etc.) about the resource',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )


class Domain(models.Model):
    """
    Specifies the application domain of the resource or the tool/service
    """

    class Meta:
        ordering = ('-pk',)

    # Fields
    scheme_name = models.CharField(
        choices=enums.DOMAIN_SCHEME,
        max_length=utils.get_max_length(enums.DOMAIN_SCHEME),
        help_text='Specifies the name of the scheme according to which the domain identifier is assigned by '
                  'the authority that issues it'
    )

    scheme_uri = models.URLField(
        max_length=100,
        help_text='Specifies the URI of the scheme according to which the domain identifier is assigned by the '
                  'authority that issues it',
        null=True,
        blank=True
    )

    value = models.CharField(max_length=300)


class FunctionInfo(models.Model):
    # Add 'Info' to name to prevent clashes with definitions elsewhere in the codebase
    """

    """

    class Meta:
        ordering = ('-pk',)

    function = models.URLField(
        choices=enums.FUNCTION,
        max_length=utils.get_max_length(enums.FUNCTION),
        help_text='Specifies the operation/function/task the application/component performs'
    )

    function_other = models.CharField(
        max_length=100,
        help_text='If none of the proposed values describes appropriately the function performed by '
                  'your resource, please select the one you consider closest and add a value (preferably '
                  'the id from another ontology) in the element functionOther. ',
        null=True,
        blank=True
    )


class GeographicClassification(models.Model):
    """
    Groups information on geographic classification of the resource
    """

    class Meta:
        ordering = ('-pk',)

    geographic_coverage = models.CharField(
        max_length=100,
        help_text='The geographic region that the content of a resource is about; for countries, recommended '
                  'use of ISO-3166'
    )

    size = models.OneToOneField(
        'repository.Size',
        help_text='Provides information on size per geographically distinct section of the resource',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    corpus = models.ForeignKey(
        'repository.Corpus',
        related_name='geographic_classifications',
        help_text='',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
    )


class Group(Actor):
    """
    Groups information on the group(s) that is/are responsible for providing further information regarding the resource
    """

    class Meta:
        ordering = ('-pk',)

    name = JSONField(
        help_text='The name(s) of the group',
        validators=[validators.validate_lang_codes]
    )

    identifiers = models.ManyToManyField(
        'repository.GroupIdentifier',
        related_name="groups",
        help_text='Reference to a persistent (recommended) identifier used for the group'
    )

    def save(self, *args, **kwargs):
        self.type = 'group'
        super().save()

    def __str__(self):
        return u'%s' % self.pk


class GroupIdentifier(models.Model):
    # Fields
    scheme_name = models.CharField(
        max_length=100,
        help_text='Specifies the name of the scheme according to which the group identifier is assigned by '
                  'the authority that issues it'
    )

    scheme_uri = models.URLField(
        max_length=100,
        help_text='Specifies the URI of the scheme according to which the group identifier is assigned by the '
                  'authority that issues it',
        null=True,
        blank=True
    )

    value = models.CharField(max_length=300)

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return u'%s' % self.pk


class Identification(models.Model):
    # Fields
    resource_name = JSONField(
        help_text='Specifies the name (full title) by which the resource is known',
        validators=[validators.validate_lang_codes]
    )

    description = JSONField(
        help_text='A short free-text account that provides information about the resource (e.g. function, contents, '
                  'technical information etc.)',
        validators=[validators.validate_lang_codes]
    )

    resource_short_name = JSONField(
        default=None,
        help_text='A short form (e.g. abbreviation, acronym etc.) used to identify the resource',
        validators=[validators.validate_lang_codes],
        null=True,
        blank=True
    )

    logo = models.URLField(
        help_text='',
        validators=[dj_validators.URLValidator],
        null=True, blank=True
    )

    # FK ResourceIdentifier

    class Meta:
        ordering = ('pk',)

    def __str__(self):
        return f'{utils.retrieve_best_lang(self.resource_name)}'


# class ImageDatasetTechnicalInfo TODO


class Language(models.Model):
    # Fields
    language_tag = models.CharField(
        max_length=30,
        help_text='The identifier of the language that is included in the resource or supported by the component, '
                  'according to the IETF BCP47 guidelines',
        editable=False
    )
    language_id = models.CharField(
        max_length=3,
        help_text='The identifier of the language subelement according to the IETF BCP47 '
                  'guidelines (i.e. ISO 639-3 codes when existing supplemented with ISO '
                  '639-3 codes for new entries)',
        validators=[validators.validate_lang_code])

    script_id = models.CharField(
        max_length=30,
        help_text='The identifier of the script subelement according to the IETF BCP47 guidelines (i.e. ISO 15924 '
                  'codes)',
        blank=True, null=True,
        validators=[validators.validate_script_code]
    )
    region_id = models.CharField(
        max_length=30,
        help_text='The identifier of the region subelement according to the IETF BCP47 guidelines '
                  '(i.e. ISO 3166 codes)',
        blank=True, null='True',
        validators=[validators.validate_region_code]
    )
    variant_id = ArrayField(
        models.CharField(max_length=30),
        help_text='The identifier of the variant subelement according to the IETF BCP47 guidelines',
        default=None,
        null=True,
        blank=True
    )

    # related fields
    metadata_header = models.ForeignKey(
        'repository.MetadataHeader',
        on_delete=models.CASCADE,
        related_name="metadata_languages",
        help_text='The language in which the metadata description is written according to IETF BCP47 (ISO 639-1 or '
                  'ISO 639-3 for languages not covered by the first standard)',
        null=True, blank=True
    )

    processing_resource = models.ForeignKey(
        'repository.ProcessingResource',
        on_delete=models.SET_NULL,
        help_text='The language that is used in the resource or supported by the component; corresponds to '
                  'language tag according to ISO 639-1 and for languages not covered by this, the ISO 639-3',
        related_name='languages',
        null=True, blank=True
    )

    class Meta:
        ordering = ('-pk',)

    def make_tag(self):
        self.language_tag = f'{self.language_id}'
        if self.script_id:
            self.language_tag += f'-{self.script_id}'
        if self.region_id:
            self.language_tag += f'-{self.region_id}'
        if self.variant_id:
            if len(self.variant_id) == 1:
                self.language_tag += f'-{self.variant_id[0]}'
            else:
                self.language_tag += "-".join(self.variant_id)

        validators.validate_language_tag(self.language_tag),

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.make_tag()
        super().save()

    def __str__(self):
        return self.language_tag


class LanguageInfo(models.Model):
    """
    Groups information on the languages represented in the resource
    """

    class Meta:
        ordering = ('-pk',)

    language = models.OneToOneField(
        'repository.Language',
        help_text='The language that is used in the resource or supported by the component; corresponds to '
                  'language tag according to ISO 639-1 and for languages not covered by this, the ISO 639-3',
        on_delete=models.CASCADE
    )

    size = models.OneToOneField(
        'repository.Size',
        help_text='Provides information on the size per language component',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    # FK language_varieties -> LanguageVariety

    corpus = models.ForeignKey(
        'repository.Corpus',
        related_name='languages',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )


class LanguageVariety(models.Model):
    """

    """

    class Meta:
        ordering = ('-pk',)

    type = models.CharField(
        choices=enums.VARIETY_TYPE,
        max_length=utils.get_max_length(enums.VARIETY_TYPE),
        help_text='Specifies the type of the language variety that occurs in the resource or is supported by '
                  'a tool/service'
    )

    name = models.CharField(
        max_length=100,
        help_text='The name of the language variety that occurs in the resource or is supported by a tool/service',
    )

    size = models.OneToOneField(
        'repository.Size',
        help_text='Provides information on the size per language variety component',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    language_info = models.ForeignKey(
        'repository.LanguageInfo',
        related_name='language_varieties',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )


class Licence(models.Model):
    """
    Base type for related licences; choice between project names and identifiers
    """

    class Meta:
        ordering = ('-pk',)

    name = JSONField(
        help_text='The name(s) of the licence',
        validators=[validators.validate_lang_codes],
        null=True,
        blank=True
    )

    identifiers = models.ManyToManyField(
        'repository.LicenceIdentifier',
        related_name="licence",
        help_text='Associates a licence to an identifier (e.g. SPDX codes etc.), i.e. a string '
                  'used to uniquely identify it'
    )


class LicenceIdentifier(models.Model):
    """
    Associates a licence to an identifier (e.g. SPDX codes etc.), i.e. a string used to uniquely identify it
    """

    class Meta:
        ordering = ('-pk',)

    scheme_name = models.CharField(
        choices=enums.LICENCE_ID_SCHEME_NAME,
        max_length=utils.get_max_length(enums.LICENCE_ID_SCHEME_NAME),
        help_text='Specifies the name of the scheme according to which the licence identifier is assigned by the '
                  'authority that issues it'
    )

    scheme_uri = models.URLField(
        max_length=100,
        help_text='Specifies the URI of the scheme according to which the licence identifier is assigned by the '
                  'authority that issues it',
        null=True,
        blank=True
    )

    value = models.CharField(max_length=300)

    def __str__(self):
        return u'%s' % self.pk


class Linguality(models.Model):
    """
    Groups information on the number of languages of the resource part and of the way they are combined to each other
    """

    class Meta:
        ordering = ('-pk',)

    linguality_type = models.CharField(
        help_text='Indicates whether the resource includes one, two or more languages',
        choices=enums.LINGUALITY_TYPE,
        max_length=utils.get_max_length(enums.LINGUALITY_TYPE)
    )

    multilinguality_type = models.CharField(
        help_text='Indicates whether the corpus is parallel, comparable or mixed',
        choices=enums.MULTILINGUALITY_TYPE,
        max_length=utils.get_max_length(enums.MULTILINGUALITY_TYPE),
        null=True,
        blank=True
    )

    multilinguality_type_details = models.CharField(
        help_text='Provides further information on multilinguality of a resource in free text',
        max_length=500,
        null=True,
        blank=True
    )


class MailingList(models.Model):
    class Meta:
        ordering = ('-pk',)

    name = models.CharField(
        max_length=100,
        help_text='The name of the mailing list'
    )
    subscribe = models.EmailField(
        max_length=100,
        help_text='The email address to be used for subscribing to the mailing list'
    )
    unsubscribe = models.EmailField(
        max_length=100,
        help_text='The email address to be used for unsubscribing from the mailing list'
    )
    post = models.EmailField(
        max_length=100,
        help_text='The email address to be used for posting messages to the mailing list'
    )
    archive = models.URLField(
        max_length=200,
        help_text='The URL where the mailing list messages are archived'
    )
    other_archives = ArrayField(
        models.URLField(max_length=100),
        help_text='Alternative ULRs where the mailing list messages are archived (for mirrored mailing lists)',
        null=True, blank=True,
        default=None
    )


class MediaPart(PolymorphicModel):
    """

    """

    class Meta:
        ordering = ('-pk',)

    media_type = models.CharField(
        max_length=utils.get_max_length(enums.MEDIA_PART_TYPE),
        choices=enums.MEDIA_PART_TYPE,
        help_text='The type of the media part',
        editable=False
    )

    def as_subclass(self):
        # pylint: disable-msg=E1101
        subclasses = self.__class__.__subclasses__()
        for subclass in subclasses:
            subclass_name = subclass.__name__
            candidate_fieldname = subclass_name.lower()
            if hasattr(self, candidate_fieldname):
                childinstance = getattr(self, candidate_fieldname)
                return childinstance.as_subclass()  # recursive
        return self

    corpus = models.ForeignKey(
        'repository.Corpus',
        related_name='corpus_parts',
        help_text='',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )


class MediaTechnicalInfo(PolymorphicModel):
    """

    """

    class Meta:
        ordering = ('-pk',)

    media_type = models.CharField(
        help_text='The media type of the corpus',
        verbose_name='Corpus Media Type',
        max_length=utils.get_max_length(enums.MEDIA_TECHNICAL_TYPES),
        choices=enums.MEDIA_TECHNICAL_TYPES,
        editable=False
    )

    def as_subclass(self):
        # pylint: disable-msg=E1101
        subclasses = self.__class__.__subclasses__()
        for subclass in subclasses:
            subclass_name = subclass.__name__
            candidate_fieldname = subclass_name.lower()
            if hasattr(self, candidate_fieldname):
                childinstance = getattr(self, candidate_fieldname)
                return childinstance.as_subclass()  # recursive
        return self

    dataset_distribution = models.ForeignKey(
        'repository.DatasetDistribution',
        related_name='technical_parts',
        help_text='',
        null=True,
        blank=True,
        on_delete=models.CASCADE
    )


class MetadataHeader(models.Model):
    # Fields
    metadata_creation_date = models.DateField(
        help_text='The date of creation of this metadata description',
        auto_now_add=True
    )
    revision = models.CharField(
        max_length=500,
        help_text='Provides an account of the revisions in free text or a link to a document with revisions',
        null=True,
        blank=True
    )

    # related fields
    metadata_record_identifier = models.OneToOneField(
        'repository.MetadataIdentifier',
        on_delete=models.CASCADE,
        related_name="metadata_header",
        help_text='A persistent identifier for the metadata record itself'
    )
    metadata_creators = models.ManyToManyField(
        'repository.Person',
        related_name="metadata_headers",
        help_text='Groups information on the persons that has created the metadata record '
                  '(for records manually edited)',
        blank=True
    )
    source_of_metadata_record = models.OneToOneField(
        'repository.SourceOfMetadataRecord',
        on_delete=models.SET_NULL,
        related_name="metadata_header",
        help_text='Refers to the catalog or repository from which the metadata record has been '
                  'originated (for harvested records)',
        null=True,
        blank=True
    )

    # FK Language

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return u'%s' % self.pk


class MetadataIdentifier(models.Model):
    # TODO: Create validators for each scheme
    # Fields
    scheme_name = models.CharField(
        max_length=utils.get_max_length(enums.METADATA_ID_SCHEME_NAME),
        choices=enums.METADATA_ID_SCHEME_NAME,
        help_text='Specifies the name of the scheme according to which the metadata record identifier is assigned by '
                  'the authority that issues it'
    )

    scheme_uri = models.URLField(
        max_length=100,
        help_text='Specifies the URI of the scheme according to which the metadata record identifier is assigned by the '
                  'authority that issues it',
        null=True,
        blank=True
    )

    value = models.CharField(max_length=300)

    class Meta:
        ordering = ('pk',)

    def __str__(self):
        return f'{self.value} - {self.scheme_name}'


class OrganizationIdentifier(models.Model):
    # Fields
    scheme_name = models.CharField(
        max_length=utils.get_max_length(enums.ORGANIZATION_ID_SCHEME),
        choices=enums.ORGANIZATION_ID_SCHEME,
        help_text='Specifies the name of the scheme according to which the organization identifier is assigned by '
                  'the authority that issues it'
    )

    scheme_uri = models.URLField(
        max_length=100,
        help_text='Specifies the URI of the scheme according to which the organization identifier is assigned by the '
                  'authority that issues it',
        null=True,
        blank=True
    )

    value = models.CharField(max_length=300)

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return u'%s' % self.pk


class PersonIdentifier(models.Model):
    # Fields
    scheme_name = models.CharField(
        max_length=utils.get_max_length(enums.PERSON_ID_SCHEME),
        choices=enums.PERSON_ID_SCHEME,
        help_text='Specifies the name of the scheme according to which the person identifier is assigned by the '
                  'authority that issues it (e.g. ORCID, ScopusId, ResearcherID etc.)'
    )

    scheme_uri = models.URLField(
        max_length=100,
        help_text='Specifies the URI of the scheme according to which the person '
                  'identifier is assigned by the authority that issues it',
        null=True,
        blank=True
    )

    value = models.CharField(max_length=300)

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return u'%s' % self.pk


class PhysicalResource(models.Model):
    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return u'%s' % self.pk


class Modality(models.Model):
    """
    Groups information on the modalities represented in the resource
    """

    class Meta:
        ordering = ('-pk',)

    type = ArrayField(
        models.CharField(
            choices=enums.MODALITY_TYPE,
            max_length=utils.get_max_length(enums.MODALITY_TYPE)
        ),
        help_text='Specifies the type of the modality represented in the resource or processed by a tool/service'
    )

    details = models.CharField(
        max_length=500,
        help_text='Provides further information on modalities',
        null=True,
        blank=True
    )

    size = models.OneToOneField(
        'repository.Size',
        help_text='Provides information on the size per modality component',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )


class Organization(Actor):
    """

    """

    class Meta:
        ordering = ('-pk',)

    # Fields
    name = JSONField(
        help_text='The name(s) of the organization',
        validators=[validators.validate_lang_codes]
    )

    # related fields
    identifiers = models.ManyToManyField(
        'repository.OrganizationIdentifier',
        related_name="persons",
        help_text='Associates an organization to an identifier, i.e. a string used to uniquely identify it'
    )

    def save(self, *args, **kwargs):
        self.type = 'organization'
        super().save()

    def __str__(self):
        return u'%s' % self.pk


class Person(Actor):
    """

    """

    class Meta:
        ordering = ('-pk',)

    # Fields
    name = JSONField(
        help_text='The name(s) of the person, preferably in the format: surname, initials (first name) prefix',
        validators=[validators.validate_lang_codes]
    )

    # related fields
    identifiers = models.ManyToManyField(
        'repository.PersonIdentifier',
        related_name="persons",
        help_text='Associates a person to an identifier (e.g. ORCID, LinkedIn etc.), i.e. a string used to uniquely '
                  'identify it'
    )

    def save(self, *args, **kwargs):
        self.type = 'person'
        super().save()

    def __str__(self):
        return u'%s' % self.pk


class ProcessingResource(models.Model):
    """

    """
    resource_type = models.CharField(
        choices=enums.PROCESSING_RESOURCE_TYPE,
        max_length=utils.get_max_length(enums.PROCESSING_RESOURCE_TYPE),
        help_text='The type of the resource that a component takes as input or produces as output'
    )

    # FK data_formats --> DataFormat

    character_encodings = ArrayField(
        models.CharField(
            choices=enums.ENCODING,
            max_length=utils.get_max_length(enums.ENCODING)
        ),
        help_text='The name of the character encoding used in the resource or supported by the component'
    )

    # FK languages --> Language
    # FK annotation_types --> AnnotationType

    type_system = models.ForeignKey(
        'repository.Resource',
        related_name='processing_type_system',
        help_text='A name or an identifier (e.g. url reference) to the typesystem used in the annotation '
                  'of the resource or used by the component',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    annotation_schema = models.ForeignKey(
        'repository.Resource',
        related_name='processing_annotation_schema',
        help_text='A set of elements and values designed to annotate data',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    annotation_resource = models.ForeignKey(
        'repository.Resource',
        related_name='processing_annotation_resource',
        help_text='A name or an identifier (e.g. url reference) to the ancillary resource '
                  '(e.g. tagset, ontology, term list) used in the annotation of the resource '
                  'or used by the component',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    samples_location = models.URLField(
        help_text='Links a resource to a url (or url\'s) with samples of the resource or, in the case of '
                  'tools, of samples of input and/or output processes',
        null=True,
        blank=True
    )

    tool_service_input = models.ForeignKey(
        'repository.ToolService',
        related_name='input_content_resources',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    tool_service_output = models.ForeignKey(
        'repository.ToolService',
        related_name='output_resources',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )


class PublicationIdentifier(models.Model):
    """
    Reference to a DOI (recommended) or any kind of identifier used for the publication
    """

    class Meta:
        ordering = ('-pk',)

    scheme_name = models.CharField(
        choices=enums.PUBLICATION_ID_SCHEME,
        max_length=utils.get_max_length(enums.PUBLICATION_ID_SCHEME),
        help_text='Specifies the name of the scheme according to which the publication identifier is assigned '
                  'by the authority that issues it (e.g. DOI, PubMed Central etc.)'
    )

    scheme_uri = models.URLField(
        max_length=100,
        help_text='',
        null=True,
        blank=True
    )

    value = models.CharField(max_length=300)

    def __str__(self):
        return u'%s' % self.pk


class Register(models.Model):
    """
    For corpora that have already been using register classification
    """

    class Meta:
        ordering = ('-pk',)

    classification_scheme_name = models.CharField(
        choices=enums.CLASSIFICATION_SCHEME,
        max_length=utils.get_max_length(enums.CLASSIFICATION_SCHEME),
        help_text=''
    )

    scheme_uri = models.URLField(
        help_text='',
        null=True,
        blank=True
    )


class Relation(models.Model):
    """
    Groups together information on relations between resources
    """

    class Meta:
        ordering = ('-pk',)

    relation_type = models.CharField(
        choices=enums.RELATION_TYPE,
        max_length=utils.get_max_length(enums.RELATION_TYPE),
        help_text='Specifies the type of relation holding between two entities (e.g. two resources that '
                  'comprise one new resource together, a corpus and the s/w component that has been used '
                  'for its creation or a corpus and the publication that describes it'
    )

    related_resource = models.ForeignKey(
        'repository.Resource',
        help_text='Reference to a resource participating in a relation; the preferred form of reference is '
                  'a unique identifier, otherwise the resource name',
        on_delete=models.PROTECT
    )

    common_info = models.ForeignKey(
        'repository.CommonInfo',
        help_text='Groups together information on relations between resources',
        related_name='relations',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )


class Repository(models.Model):
    """

    """

    class Meta:
        ordering = ('-pk',)

    # Fields
    name = JSONField(
        default=None,
        help_text='The name(s) of the repository',
        validators=[validators.validate_lang_codes]
    )

    # related fields
    identifiers = models.ManyToManyField(
        'repository.RepositoryIdentifier',
        related_name="repositories",
        help_text='Reference to the identifiers used for repositories, archives etc.'
    )

    def __str__(self):
        return u'%s' % self.pk


class RepositoryIdentifier(models.Model):
    # Fields
    scheme_name = models.CharField(
        choices=enums.REPOSITORY_ID_SCHEME_NAME,
        max_length=utils.get_max_length(enums.REPOSITORY_ID_SCHEME_NAME),
    )

    scheme_uri = models.URLField(
        max_length=100,
        help_text='Specifies the URI of the scheme according to which the repository identifier is assigned by '
                  'the authority that issues it',
        null=True,
        blank=True
    )

    value = models.CharField(max_length=300)

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return u'%s' % self.pk


class Resource(models.Model):
    """
    Base type for related resources; sequence of resource names and identifiers
    """

    class Meta:
        ordering = ('-pk',)

    name = JSONField(
        help_text='Specifies the name (full title) by which the resource is known',
        validators=[validators.validate_lang_codes]
    )

    # related fields
    identifiers = models.ManyToManyField(
        'repository.ResourceIdentifier',
        related_name="repositories",
        help_text='Reference to a DOI (recommended) or any kind of identifier used for the resource'
    )

    component_dependency = models.ForeignKey(
        'repository.ComponentDependency',
        related_name='annotation_resources',
        help_text='A name or an identifier (e.g. url reference) to the ancillary resource '
                  '(e.g. tagset, ontology, term list) used in the annotation of the resource '
                  'or used by the component',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    tool_service_creation = models.ForeignKey(
        'repository.ToolServiceCreation',
        related_name='original_sources',
        help_text='The names, the identifiers or the urls of the original resources that were at the base of '
                  'the creation process of the resource',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )


class ResourceIdentifier(models.Model):
    # Fields
    scheme_name = models.CharField(
        choices=enums.RESOURCE_ID_SCHEME_NAME,
        max_length=utils.get_max_length(enums.RESOURCE_ID_SCHEME_NAME),
        help_text='Specifies the name of the scheme according to which the resource identifier is assigned by the '
                  'authority that issues it (e.g. DOI, ISLRN etc.)'
    )

    scheme_uri = models.CharField(
        max_length=100,
        help_text='Specifies the URI of the scheme according to which the resource identifier is assigned by the '
                  'authority that issues it',
        blank=True, null=True
    )

    value = models.CharField(max_length=300)

    # related fields
    identification = models.ForeignKey(
        'repository.Identification',
        on_delete=models.CASCADE,
        related_name="resource_identifiers",
        help_text='Associates a resource to an identifier (e.g. PID, DOI, internal to an organization etc.), '
                  'i.e. a string used to uniquely identify it ',
        null=True,
        blank=True
    )

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return u'%s' % self.pk


class ResourceRights(models.Model):
    """
    Groups information on copyright for a resource
    """

    class Meta:
        ordering = ('-pk',)

    rights_statement = models.CharField(
        help_text='The name (identifier) of a formal statement indicative of licensing terms for the use of a '
                  'resource (e.g. open access, free to read etc.); its semantics should be clear, preferrably '
                  'formally expressed and stored at a URL',
        max_length=utils.get_max_length(enums.RIGHTS_STATEMENT),
        choices=enums.RIGHTS_STATEMENT
    )

    copyright_statement = models.CharField(
        help_text='A free text statement that may be included with the resource, usually containing the name(s) '
                  'of copyright holders and licensing terms (intended for resources that already have such '
                  'a statement).',
        max_length=2000,
        null=True, blank=True
    )

    rights_holders = models.ManyToManyField(
        'repository.Actor',
        help_text='',
    )


class Size(models.Model):
    """
    Groups information on the size of the resource or of resource parts
    """

    class Meta:
        ordering = ('-pk',)

    size = models.FloatField(
        help_text='Specifies the size of the resource with regard to the SizeUnit measurement in form of a number'
    )

    size_unit = models.CharField(
        choices=enums.SIZE_UNIT,
        max_length=utils.get_max_length(enums.SIZE_UNIT),
        help_text='Specifies the unit that is used when providing information on the size of the resource or of '
                  'resource parts'
    )

    # FK TextDatasetTechnicalInfo
    text_dataset_technical_info = models.ForeignKey(
        'repository.TextDatasetTechnicalInfo',
        help_text='Groups information on the size of the resource or of resource parts',
        related_name='sizes',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )


class TextFormat(models.Model):
    """
    Groups information on the text format(s) of a resource
    """

    class Meta:
        ordering = ('-pk',)

    data_format = models.OneToOneField(
        'repository.DataFormat',
        help_text='Specifies the format that is used since often the mime type will not be sufficient for '
                  'machine processing; NOTE: normally the format should be represented as a combination of '
                  'the mimetype (e.g. application/xml) and some name and link to the documentation about the '
                  'supplementary conventions used (e.g xces, alvisED etc.)',
        on_delete=models.CASCADE
    )

    size = models.OneToOneField(
        'repository.Size',
        help_text='Provides information on the size of the resource parts with different format',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    # FK TextDatasetTechnicalInfo
    text_dataset_technical_info = models.ForeignKey(
        'repository.TextDatasetTechnicalInfo',
        help_text='Groups information on the text format(s) of a resource',
        related_name='text_formats',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )


class TextPart(MediaPart):
    """

    """

    class Meta:
        ordering = ('-pk',)

    creation = models.OneToOneField(
        'repository.Creation',
        help_text='Groups together information on the resource creation (e.g. for corpora, selection of '
                  'texts/audio files/ video files etc.; for lexica, construction of lemma list etc.)',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    modality = models.OneToOneField(
        'repository.Modality',
        help_text='Groups information on the modalities represented in the resource',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    # FK text_classifications -> TextClassification
    # FK annotations -> Annotation

    # link_to_other_media TODO

    def save(self, *args, **kwargs):
        self.type = 'text'
        super().save()


class TimeClassification(models.Model):
    """
    Groups together information on time classification of the resource
    """

    class Meta:
        ordering = ('-pk',)

    time_coverage = models.CharField(
        max_length=100,
        help_text='The time period that the content of a resource is about'
    )

    size = models.OneToOneField(
        'repository.Size',
        help_text='Provides information on size per time period represented in the resource',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    corpus = models.ForeignKey(
        'repository.Corpus',
        related_name='time_classifications',
        help_text='',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
    )


class ToolServiceCreation(models.Model):
    """
        Groups together information on the creation of a component
        """

    class Meta:
        ordering = ('-pk',)

    framework = models.CharField(
        choices=enums.FRAMEWORK,
        max_length=utils.get_max_length(enums.FRAMEWORK),
        help_text='The framework used for developing and deploying the component',
        null=True,
        blank=True
    )

    formalism = models.CharField(
        max_length=100,
        help_text='Reference (name, bibliographic reference or link to url) for the formalism used '
                  'for the creation/enrichment of the resource (grammar or tool/service)',
        null=True,
        blank=True
    )

    tdm_method = models.CharField(
        choices=enums.TDM_METHOD,
        max_length=utils.get_max_length(enums.TDM_METHOD),
        help_text='',
        null=True,
        blank=True
    )

    implementation_language = models.CharField(
        max_length=100,
        help_text='The programming languages needed for allowing user contributions, or for running '
                  'the tools, in case no executables are available',
        null=True,
        blank=True
    )

    # FK original_sources --> Resource

    creation_details = models.CharField(
        max_length=500,
        help_text='Provides additional information on the creation of a component',
        null=True,
        blank=True
    )


class ToolService(LanguageResource):
    """
    Groups together all information related to software tools or services
    """

    class Meta:
        ordering = ('-pk',)

    function_info = models.OneToOneField(
        'repository.FunctionInfo',  # Add 'Info' to name to prevent clashes with definitions elsewhere in the codebase
        help_text='',
        on_delete=models.CASCADE
    )

    # FK distributions --> ComponentDistribution
    # FK input_content_resources --> ProcessingResource
    # FK output_resources --> ProcessingResource
    # FK dependencies --> ComponentDependency

    creation = models.OneToOneField(
        'repository.ToolServiceCreation',
        help_text='Groups together information on the creation of a component',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    def save(self, *args, **kwargs):
        self.resource_type = 'toolService'
        super().save()


class SourceOfMetadataRecord(models.Model):
    """

    """

    class Meta:
        ordering = ('-pk',)

    # Fields
    source_metadata_link = models.URLField(
        help_text='A link to the original metadata record, in cases of harvesting',
        validators=[dj_validators.URLValidator]
    )

    collected_from = models.ForeignKey(
        'repository.Repository',
        on_delete=models.CASCADE,
        related_name="source_of_metadata_record",
        help_text='Reference to the identifier used for repositories, archives etc.'
    )

    def __str__(self):
        return u'%s' % self.pk


class Subject(models.Model):
    """
    For corpora that have already been using subject classification
    """

    class Meta:
        ordering = ('-pk',)

    classification_scheme_name = models.CharField(
        choices=enums.CLASSIFICATION_SCHEME,
        max_length=utils.get_max_length(enums.CLASSIFICATION_SCHEME),
        help_text=''
    )

    scheme_uri = models.URLField(
        help_text='',
        null=True,
        blank=True
    )


class TextClassification(models.Model):
    """
    Groups together information on text type/genre of the resource
    """

    class Meta:
        ordering = ('-pk',)

    keywords = ArrayField(
        models.CharField(max_length=50),
        help_text='For corpora that have already been using keywords classification'
    )

    text_genre = models.OneToOneField(
        'repository.TextGenre',
        help_text='Genre: The conventionalized discourse or text types of the content of the resource, '
                  'based on extra-linguistic and internal linguistic criteria',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    text_type = models.OneToOneField(
        'repository.TextType',
        help_text='Specifies the type of the text according to a text type classification',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    register = models.OneToOneField(
        'repository.Register',
        help_text='For corpora that have already been using register classification',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    subject = models.OneToOneField(
        'repository.Subject',
        help_text='For corpora that have already been using subject classification',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    size = models.OneToOneField(
        'repository.Size',
        help_text='Provides information on size of resource parts with different text classification',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    text_part = models.ForeignKey(
        'repository.TextPart',
        related_name='text_classifications',
        help_text='',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )


class TextDatasetTechnicalInfo(MediaTechnicalInfo):
    """

    """

    class Meta:
        ordering = ('-pk',)

    # FK sizes --> Size
    # FK text_formats --> TextFormat
    # FK character_encodings --> CharacterEncoding

    def save(self, *args, **kwargs):
        self.media_type = 'text'
        super().save()


class TextGenre(models.Model):
    """
    Genre: The conventionalized discourse or text types of the content of the resource, based on extra-linguistic
    and internal linguistic criteria
    """

    class Meta:
        ordering = ('-pk',)

    classification_scheme_name = models.CharField(
        choices=enums.CLASSIFICATION_SCHEME,
        max_length=utils.get_max_length(enums.CLASSIFICATION_SCHEME),
        help_text=''
    )

    scheme_uri = models.URLField(
        help_text='',
        null=True,
        blank=True
    )


class TextType(models.Model):
    """
    Genre: The conventionalized discourse or text types of the content of the resource, based on extra-linguistic
    and internal linguistic criteria
    """

    class Meta:
        ordering = ('-pk',)

    classification_scheme_name = models.CharField(
        choices=enums.CLASSIFICATION_SCHEME,
        max_length=utils.get_max_length(enums.CLASSIFICATION_SCHEME),
        help_text=''
    )

    scheme_uri = models.URLField(
        help_text='',
        null=True,
        blank=True
    )


class Version(models.Model):
    # Fields
    version = models.CharField(
        max_length=30, help_text='Associates a resource with a numeric pattern of the form '
                                 'major_version.minor_version.patch (in accordance to the '
                                 'semantic versioning guidelines, cf. http://semver.org) that '
                                 'identifies its version'
    )
    digest = models.CharField(max_length=200, blank=True, null=True)
    date = models.DateField(
        help_text='Associates a resource with the last modification date (or creation date if it hasn\'t been '
                  'modified); the preferred format is YYYYMMDD',
        blank=True, null=True
    )
    type = models.CharField(
        choices=enums.VERSION_TYPE,
        max_length=utils.get_max_length(enums.VERSION_TYPE),
        help_text='A type of versioning vocabulary used mainly for works (e.g. scholarly articles, books etc.) that '
                  'go through a formal review process before final publication, marked with major changes across the '
                  'versions',
        blank=True, null=True
    )
    revision = models.CharField(
        max_length=500,
        help_text='A short free-text account of the revisions made to the current version vis-a-vis the previous '
                  'version or a link to a document reporting these revisions',
        blank=True, null=True
    )

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return u'%s' % self.pk

# class VideoDatasetTechnicalInfo TODO
