from django.core.exceptions import ValidationError as Django_ValidationError
from django.core.validators import URLValidator, EmailValidator, RegexValidator
from language_tags import tags
from rest_framework.exceptions import ValidationError


def validate_lang_code(code):
    if not tags.check(code):
        raise ValidationError(f'\'{code}\' is not a valid language code (https://tools.ietf.org/html/bcp47)')


def validate_lang_codes(code_dict):
    """validates codes that are keys in a dict"""

    for key in code_dict.keys():
        validate_lang_code(key)


def validate_language_tag(tag):
    """BCP47 Validation"""
    if tags.tag(tag).errors:
        messages = [err.message for err in tags.tag(tag).errors]
        raise ValidationError(messages)


def validate_region_code(region):
    if not tags.region(region):
        raise ValidationError(f'\'{region}\' is not a valid region code (https://tools.ietf.org/html/bcp47)')


def validate_script_code(script):
    if not tags.subtags(script.lower()):
        raise ValidationError(f'\'{script}\' is not a valid script code (https://tools.ietf.org/html/bcp47)')


def validate_url(value):
    url_validator = URLValidator()
    try:
        url_validator.__call__(value)
    except Django_ValidationError:
        raise ValidationError(f"The value '{value}' is not a valid url")


def validate_email(value):
    email_validator = EmailValidator()
    try:
        email_validator.__call__(value)
    except Django_ValidationError:
        raise ValidationError(f"The value '{value}' is not a valid email address")


def validate_orcid(value):
    # https://orcid.org/0000-000x-xxxx-xxxx.
    # https://orcid.org/0000-0001-2345-6789
    return RegexValidator(
        regex=r'^https:\/\/orcid.org\/0000-000\d-\d{4}-\d{3}[0-9X]$',
        message='Use the format: https://orcid.org/0000-000x-xxxx-xxxx.'
    )(value)
