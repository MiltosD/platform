from rest_framework import viewsets, permissions, status
from rest_framework.response import Response

from repository import models
from repository.api.serializers import RepositorySerializer
from repository.models import Repository
from . import serializers


class ResourceMetadataRecordViewSet(viewsets.ModelViewSet):
    """ViewSet for the ResourceMetadataRecord class"""

    queryset = models.ResourceMetadataRecord.objects.all()
    serializer_class = serializers.ResourceMetadataRecordSerializer
    permission_classes = [permissions.IsAuthenticated]


class ActorViewSet(viewsets.ModelViewSet):
    """ViewSet for the Actor class"""

    queryset = models.Actor.objects.all()
    serializer_class = serializers.ActorSerializer
    permission_classes = [permissions.IsAuthenticated]


class AnnotationViewSet(viewsets.ModelViewSet):
    """ViewSet for the Annotation class"""

    queryset = models.Annotation.objects.all()
    serializer_class = serializers.AnnotationSerializer
    permission_classes = [permissions.IsAuthenticated]


class AnnotationTypeViewSet(viewsets.ModelViewSet):
    """ViewSet for the AnnotationType class"""

    queryset = models.AnnotationType.objects.all()
    serializer_class = serializers.AnnotationTypeSerializer
    permission_classes = [permissions.IsAuthenticated]


class CharacterEncodingViewSet(viewsets.ModelViewSet):
    """ViewSet for the CharacterEncoding class"""

    queryset = models.CharacterEncoding.objects.all()
    serializer_class = serializers.CharacterEncodingSerializer
    permission_classes = [permissions.IsAuthenticated]


class CommonInfoViewSet(viewsets.ModelViewSet):
    """ViewSet for the CommonInfo class"""

    queryset = models.CommonInfo.objects.all()
    serializer_class = serializers.CommonInfoSerializer
    permission_classes = [permissions.IsAuthenticated]


class ComponentDependencyViewSet(viewsets.ModelViewSet):
    """ViewSet for the ComponentDependency class"""

    queryset = models.ComponentDependency.objects.all()
    serializer_class = serializers.ComponentDependencySerializer
    permission_classes = [permissions.IsAuthenticated]


class ComponentDistributionViewSet(viewsets.ModelViewSet):
    """ViewSet for the ComponentDistribution class"""

    queryset = models.ComponentDistribution.objects.all()
    serializer_class = serializers.ComponentDistributionSerializer
    permission_classes = [permissions.IsAuthenticated]


class ContactViewSet(viewsets.ModelViewSet):
    """ViewSet for the Contact class"""

    queryset = models.Contact.objects.all()
    serializer_class = serializers.ContactSerializer
    permission_classes = [permissions.IsAuthenticated]


class CorpusViewSet(viewsets.ModelViewSet):
    """ViewSet for the Corpus class"""

    queryset = models.Corpus.objects.all()
    serializer_class = serializers.CorpusSerializer
    permission_classes = [permissions.IsAuthenticated]


class ToolServiceViewSet(viewsets.ModelViewSet):
    """ViewSet for the ToolService class"""

    queryset = models.ToolService.objects.all()
    serializer_class = serializers.ToolServiceSerializer
    permission_classes = [permissions.IsAuthenticated]


class ToolServiceCreationViewSet(viewsets.ModelViewSet):
    """ViewSet for the ToolServiceCreation class"""

    queryset = models.ToolServiceCreation.objects.all()
    serializer_class = serializers.ToolServiceCreationSerializer
    permission_classes = [permissions.IsAuthenticated]


class LanguageResourceViewSet(viewsets.ModelViewSet):
    """ViewSet for the LanguageResource class"""

    queryset = models.LanguageResource.objects.all()
    serializer_class = serializers.LanguageResourceSerializer
    permission_classes = [permissions.IsAuthenticated]


class DatasetDistributionViewSet(viewsets.ModelViewSet):
    """ViewSet for the DatasetDistribution class"""

    queryset = models.DatasetDistribution.objects.all()
    serializer_class = serializers.DatasetDistributionSerializer
    permission_classes = [permissions.IsAuthenticated]


class DateViewSet(viewsets.ModelViewSet):
    """ViewSet for the Date class"""

    queryset = models.Date.objects.all()
    serializer_class = serializers.DateSerializer
    permission_classes = [permissions.IsAuthenticated]


class DateRangeTypeViewSet(viewsets.ModelViewSet):
    """ViewSet for the DateRangeType class"""

    queryset = models.DateRangeType.objects.all()
    serializer_class = serializers.DateRangeTypeSerializer
    permission_classes = [permissions.IsAuthenticated]


class DateCombinationViewSet(viewsets.ModelViewSet):
    """ViewSet for the DateCombination class"""

    queryset = models.DateCombination.objects.all()
    serializer_class = serializers.DateCombinationSerializer
    permission_classes = [permissions.IsAuthenticated]


class DistributionRightsViewSet(viewsets.ModelViewSet):
    """ViewSet for the DistributionRights class"""

    queryset = models.DistributionRights.objects.all()
    serializer_class = serializers.DistributionRightsSerializer
    permission_classes = [permissions.IsAuthenticated]


class CreationViewSet(viewsets.ModelViewSet):
    """ViewSet for the Creation class"""

    queryset = models.Creation.objects.all()
    serializer_class = serializers.CreationSerializer
    permission_classes = [permissions.IsAuthenticated]


class DataFormatViewSet(viewsets.ModelViewSet):
    """ViewSet for the DataFormat class"""

    queryset = models.DataFormat.objects.all()
    serializer_class = serializers.DataFormatSerializer
    permission_classes = [permissions.IsAuthenticated]


class DocumentViewSet(viewsets.ModelViewSet):
    """ViewSet for the Document class"""

    queryset = models.Document.objects.all()
    serializer_class = serializers.DocumentSerializer
    permission_classes = [permissions.IsAuthenticated]


class DocumentationViewSet(viewsets.ModelViewSet):
    """ViewSet for the Document class"""

    queryset = models.Documentation.objects.all()
    serializer_class = serializers.DocumentationSerializer
    permission_classes = [permissions.IsAuthenticated]


class DomainViewSet(viewsets.ModelViewSet):
    """ViewSet for the Document class"""

    queryset = models.Domain.objects.all()
    serializer_class = serializers.DomainSerializer
    permission_classes = [permissions.IsAuthenticated]


class FunctionInfoViewSet(viewsets.ModelViewSet):
    """ViewSet for the FunctionInfo class"""

    queryset = models.FunctionInfo.objects.all()
    serializer_class = serializers.FunctionInfoSerializer
    permission_classes = [permissions.IsAuthenticated]


class GeographicClassificationViewSet(viewsets.ModelViewSet):
    """ViewSet for the GeographicClassification class"""

    queryset = models.GeographicClassification.objects.all()
    serializer_class = serializers.GeographicClassificationSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """ViewSet for the GroupIdentifier class"""

    queryset = models.Group.objects.all()
    serializer_class = serializers.GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupIdentifierViewSet(viewsets.ModelViewSet):
    """ViewSet for the GroupIdentifier class"""

    queryset = models.GroupIdentifier.objects.all()
    serializer_class = serializers.GroupIdentifierSerializer
    permission_classes = [permissions.IsAuthenticated]


class IdentificationViewSet(viewsets.ModelViewSet):
    """ViewSet for the Identification class"""

    queryset = models.Identification.objects.all()
    serializer_class = serializers.IdentificationSerializer
    permission_classes = [permissions.IsAuthenticated]


class LanguageViewSet(viewsets.ModelViewSet):
    """ViewSet for the Language class"""

    queryset = models.Language.objects.all()
    serializer_class = serializers.LanguageSerializer
    permission_classes = [permissions.IsAuthenticated]


class LanguageInfoViewSet(viewsets.ModelViewSet):
    """ViewSet for the LanguageInfo class"""

    queryset = models.LanguageInfo.objects.all()
    serializer_class = serializers.LanguageInfoSerializer
    permission_classes = [permissions.IsAuthenticated]


class LanguageVarietyViewSet(viewsets.ModelViewSet):
    """ViewSet for the LanguageVariety class"""

    queryset = models.LanguageVariety.objects.all()
    serializer_class = serializers.LanguageVarietySerializer
    permission_classes = [permissions.IsAuthenticated]


class LicenceViewSet(viewsets.ModelViewSet):
    """ViewSet for the Licence class"""

    queryset = models.Licence.objects.all()
    serializer_class = serializers.LicenceSerializer
    permission_classes = [permissions.IsAuthenticated]


class LicenceIdentifierViewSet(viewsets.ModelViewSet):
    """ViewSet for the LicenceIdentifier class"""

    queryset = models.LicenceIdentifier.objects.all()
    serializer_class = serializers.LicenceIdentifierSerializer
    permission_classes = [permissions.IsAuthenticated]


class LingualityViewSet(viewsets.ModelViewSet):
    """ViewSet for the Linguality class"""

    queryset = models.Linguality.objects.all()
    serializer_class = serializers.LingualitySerializer
    permission_classes = [permissions.IsAuthenticated]


class MailingListViewSet(viewsets.ModelViewSet):
    """ViewSet for the MailingList class"""

    queryset = models.MailingList.objects.all()
    serializer_class = serializers.MailingListSerializer
    permission_classes = [permissions.IsAuthenticated]


class MediaPartViewSet(viewsets.ModelViewSet):
    """ViewSet for the MediaPart class"""

    queryset = models.MediaPart.objects.all()
    serializer_class = serializers.MediaPartSerializer
    permission_classes = [permissions.IsAuthenticated]


class TextDatasetTechnicalInfoViewSet(viewsets.ModelViewSet):
    """ViewSet for the TextDatasetTechnicalInfo class"""

    queryset = models.TextDatasetTechnicalInfo.objects.all()
    serializer_class = serializers.TextDatasetTechnicalInfoSerializer
    permission_classes = [permissions.IsAuthenticated]


class MediaTechnicalInfoViewSet(viewsets.ModelViewSet):
    """ViewSet for the MediaTechnicalInfo class"""

    queryset = models.MediaTechnicalInfo.objects.all()
    serializer_class = serializers.MediaTechnicalInfoSerializer
    permission_classes = [permissions.IsAuthenticated]


class MetadataHeaderViewSet(viewsets.ModelViewSet):
    """ViewSet for the MetadataHeader class"""

    queryset = models.MetadataHeader.objects.all()
    serializer_class = serializers.MetadataHeaderSerializer
    permission_classes = [permissions.IsAuthenticated]


class MetadataIdentifierViewSet(viewsets.ModelViewSet):
    """ViewSet for the MetadataIdentifier class"""

    queryset = models.MetadataIdentifier.objects.all()
    serializer_class = serializers.MetadataIdentifierSerializer
    permission_classes = [permissions.DjangoModelPermissions]


class OrganizationIdentifierViewSet(viewsets.ModelViewSet):
    """ViewSet for the OrganizationIdentifier class"""

    queryset = models.OrganizationIdentifier.objects.all()
    serializer_class = serializers.OrganizationIdentifierSerializer
    permission_classes = [permissions.IsAuthenticated]


class OrganizationViewSet(viewsets.ModelViewSet):
    """ViewSet for the PersonIdentifier class"""

    queryset = models.Organization.objects.all()
    serializer_class = serializers.OrganizationSerializer
    permission_classes = [permissions.IsAuthenticated]


class PersonViewSet(viewsets.ModelViewSet):
    """ViewSet for the PersonIdentifier class"""

    queryset = models.Person.objects.all()
    serializer_class = serializers.PersonSerializer
    permission_classes = [permissions.IsAuthenticated]


class PersonIdentifierViewSet(viewsets.ModelViewSet):
    """ViewSet for the PersonIdentifier class"""

    queryset = models.PersonIdentifier.objects.all()
    serializer_class = serializers.PersonIdentifierSerializer
    permission_classes = [permissions.IsAuthenticated]


class PhysicalResourceViewSet(viewsets.ModelViewSet):
    """ViewSet for the PhysicalResource class"""

    queryset = models.PhysicalResource.objects.all()
    serializer_class = serializers.PhysicalResourceSerializer
    permission_classes = [permissions.IsAuthenticated]


class ModalityViewSet(viewsets.ModelViewSet):
    """ViewSet for the Modality class"""

    queryset = models.Modality.objects.all()
    serializer_class = serializers.ModalitySerializer
    permission_classes = [permissions.IsAuthenticated]


class ProcessingResourceViewSet(viewsets.ModelViewSet):
    """ViewSet for the ProcessingResource class"""

    queryset = models.ProcessingResource.objects.all()
    serializer_class = serializers.ProcessingResourceSerializer
    permission_classes = [permissions.IsAuthenticated]


class PublicationIdentifierViewSet(viewsets.ModelViewSet):
    """ViewSet for the PublicationIdentifier class"""

    queryset = models.PublicationIdentifier.objects.all()
    serializer_class = serializers.PublicationIdentifierSerializer
    permission_classes = [permissions.IsAuthenticated]


class RegisterViewSet(viewsets.ModelViewSet):
    """ViewSet for the Register class"""

    queryset = models.Register.objects.all()
    serializer_class = serializers.RegisterSerializer
    permission_classes = [permissions.IsAuthenticated]


class RelationViewSet(viewsets.ModelViewSet):
    """ViewSet for the Relation class"""

    queryset = models.Relation.objects.all()
    serializer_class = serializers.RelationSerializer
    permission_classes = [permissions.IsAuthenticated]


class RepositoryViewSet(viewsets.ModelViewSet):
    """ViewSet for the Repository class"""

    queryset = models.Repository.objects.all()
    serializer_class = serializers.RepositorySerializer
    permission_classes = [permissions.IsAuthenticated]


class RepositoryIdentifierViewSet(viewsets.ModelViewSet):
    """ViewSet for the RepositoryIdentifier class"""

    queryset = models.RepositoryIdentifier.objects.all()
    serializer_class = serializers.RepositoryIdentifierSerializer
    permission_classes = [permissions.IsAuthenticated]


class ResourceViewSet(viewsets.ModelViewSet):
    """ViewSet for the Resource class"""

    queryset = models.Resource.objects.all()
    serializer_class = serializers.ResourceSerializer
    permission_classes = [permissions.IsAuthenticated]


class ResourceIdentifierViewSet(viewsets.ModelViewSet):
    """ViewSet for the ResourceIdentifier class"""

    queryset = models.ResourceIdentifier.objects.all()
    serializer_class = serializers.ResourceIdentifierSerializer
    permission_classes = [permissions.IsAuthenticated]


class ResourceRightsViewSet(viewsets.ModelViewSet):
    """ViewSet for the ResourceRights class"""

    queryset = models.ResourceRights.objects.all()
    serializer_class = serializers.ResourceRightsSerializer
    permission_classes = [permissions.IsAuthenticated]


class SizeViewSet(viewsets.ModelViewSet):
    """ViewSet for the Size class"""

    queryset = models.Size.objects.all()
    serializer_class = serializers.SizeSerializer
    permission_classes = [permissions.IsAuthenticated]


class TextFormatViewSet(viewsets.ModelViewSet):
    """ViewSet for the TextFormat class"""

    queryset = models.TextFormat.objects.all()
    serializer_class = serializers.TextFormatSerializer
    permission_classes = [permissions.IsAuthenticated]


class TextPartViewSet(viewsets.ModelViewSet):
    """ViewSet for the TextPart class"""

    queryset = models.TextPart.objects.all()
    serializer_class = serializers.TextPartSerializer
    permission_classes = [permissions.IsAuthenticated]


class TimeClassificationViewSet(viewsets.ModelViewSet):
    """ViewSet for the TimeClassification class"""

    queryset = models.TimeClassification.objects.all()
    serializer_class = serializers.TimeClassificationSerializer
    permission_classes = [permissions.IsAuthenticated]


class SourceOfMetadataRecordViewSet(viewsets.ModelViewSet):
    """ViewSet for the SourceOfMetadataRecord class"""

    queryset = models.SourceOfMetadataRecord.objects.all()
    serializer_class = serializers.SourceOfMetadataRecordSerializer
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        if isinstance(request.data.get('collected_from'), int):
            try:
                repository = Repository.objects.get(id=request.data.get('collected_from'))
                request.data['collected_from'] = RepositorySerializer(repository).data
            except Repository.DoesNotExist:
                return Response({"collected_from": f" repository({request.data.get('collected_from')}) Not found."},
                                status=status.HTTP_400_BAD_REQUEST)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        if isinstance(request.data.get('collected_from'), int):
            try:
                repository = Repository.objects.get(id=request.data.get('collected_from'))
                request.data['collected_from'] = RepositorySerializer(repository).data
            except Repository.DoesNotExist:
                return Response({"collected_from": f" repository({request.data.get('collected_from')}) Not found."},
                                status=status.HTTP_400_BAD_REQUEST)

        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)


class SubjectViewSet(viewsets.ModelViewSet):
    """ViewSet for the Subject class"""

    queryset = models.Subject.objects.all()
    serializer_class = serializers.SubjectSerializer
    permission_classes = [permissions.IsAuthenticated]


class TextClassificationViewSet(viewsets.ModelViewSet):
    """ViewSet for the TextClassification class"""

    queryset = models.TextClassification.objects.all()
    serializer_class = serializers.TextClassificationSerializer
    permission_classes = [permissions.IsAuthenticated]


class TextGenreViewSet(viewsets.ModelViewSet):
    """ViewSet for the TextGenre class"""

    queryset = models.TextGenre.objects.all()
    serializer_class = serializers.TextGenreSerializer
    permission_classes = [permissions.IsAuthenticated]


class TextTypeViewSet(viewsets.ModelViewSet):
    """ViewSet for the TextType class"""

    queryset = models.TextType.objects.all()
    serializer_class = serializers.TextTypeSerializer
    permission_classes = [permissions.IsAuthenticated]


class VersionViewSet(viewsets.ModelViewSet):
    """ViewSet for the Version class"""

    queryset = models.Version.objects.all()
    serializer_class = serializers.VersionSerializer
    permission_classes = [permissions.IsAuthenticated]
