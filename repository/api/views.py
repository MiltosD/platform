from django.views.generic import DetailView, ListView, UpdateView, CreateView

from repository import models


class ResourceMetadataRecordListView(ListView):
    model = models.ResourceMetadataRecord


class ResourceMetadataRecordCreateView(CreateView):
    model = models.ResourceMetadataRecord


class ResourceMetadataRecordDetailView(DetailView):
    model = models.ResourceMetadataRecord


class ResourceMetadataRecordUpdateView(UpdateView):
    model = models.ResourceMetadataRecord


class ActorListView(ListView):
    model = models.Actor


class ActorCreateView(CreateView):
    model = models.Actor


class ActorDetailView(DetailView):
    model = models.Actor


class ActorUpdateView(UpdateView):
    model = models.Actor


class AnnotationListView(ListView):
    model = models.Annotation


class AnnotationCreateView(CreateView):
    model = models.Annotation


class AnnotationDetailView(DetailView):
    model = models.Annotation


class AnnotationUpdateView(UpdateView):
    model = models.Annotation


class AnnotationTypeListView(ListView):
    model = models.AnnotationType


class AnnotationTypeCreateView(CreateView):
    model = models.AnnotationType


class AnnotationTypeDetailView(DetailView):
    model = models.AnnotationType


class AnnotationTypeUpdateView(UpdateView):
    model = models.AnnotationType


class CharacterEncodingListView(ListView):
    model = models.CharacterEncoding


class CharacterEncodingCreateView(CreateView):
    model = models.CharacterEncoding


class CharacterEncodingDetailView(DetailView):
    model = models.CharacterEncoding


class CharacterEncodingUpdateView(UpdateView):
    model = models.CharacterEncoding


class CommonInfoListView(ListView):
    model = models.CommonInfo


class CommonInfoCreateView(CreateView):
    model = models.CommonInfo


class CommonInfoDetailView(DetailView):
    model = models.CommonInfo


class CommonInfoUpdateView(UpdateView):
    model = models.CommonInfo


class ComponentDependencyListView(ListView):
    model = models.ComponentDependency


class ComponentDependencyCreateView(CreateView):
    model = models.ComponentDependency


class ComponentDependencyDetailView(DetailView):
    model = models.ComponentDependency


class ComponentDependencyUpdateView(UpdateView):
    model = models.ComponentDependency


class ComponentDistributionListView(ListView):
    model = models.ComponentDistribution


class ComponentDistributionCreateView(CreateView):
    model = models.ComponentDistribution


class ComponentDistributionDetailView(DetailView):
    model = models.ComponentDistribution


class ComponentDistributionUpdateView(UpdateView):
    model = models.ComponentDistribution


class ContactListView(ListView):
    model = models.Contact


class ContactCreateView(CreateView):
    model = models.Contact


class ContactDetailView(DetailView):
    model = models.Contact


class ContactUpdateView(UpdateView):
    model = models.Contact


class LanguageResourceListView(ListView):
    model = models.LanguageResource


class LanguageResourceCreateView(CreateView):
    model = models.LanguageResource


class LanguageResourceDetailView(DetailView):
    model = models.LanguageResource


class LanguageResourceUpdateView(UpdateView):
    model = models.LanguageResource


class DatasetDistributionListView(ListView):
    model = models.DatasetDistribution


class DatasetDistributionCreateView(CreateView):
    model = models.DatasetDistribution


class DatasetDistributionDetailView(DetailView):
    model = models.DatasetDistribution


class DatasetDistributionUpdateView(UpdateView):
    model = models.DatasetDistribution


class DateListView(ListView):
    model = models.Date


class DateCreateView(CreateView):
    model = models.Date


class DateDetailView(DetailView):
    model = models.Date


class DateUpdateView(UpdateView):
    model = models.Date


class DateRangeTypeListView(ListView):
    model = models.DateRangeType


class DateRangeTypeCreateView(CreateView):
    model = models.DateRangeType


class DateRangeTypeDetailView(DetailView):
    model = models.DateRangeType


class DateRangeTypeUpdateView(UpdateView):
    model = models.DateRangeType


class DateCombinationListView(ListView):
    model = models.DateCombination


class DateCombinationCreateView(CreateView):
    model = models.DateCombination


class DateCombinationDetailView(DetailView):
    model = models.DateCombination


class DateCombinationUpdateView(UpdateView):
    model = models.DateCombination


class DistributionRightsListView(ListView):
    model = models.DistributionRights


class DistributionRightsCreateView(CreateView):
    model = models.DistributionRights


class DistributionRightsDetailView(DetailView):
    model = models.DistributionRights


class DistributionRightsUpdateView(UpdateView):
    model = models.DistributionRights


class CreationListView(ListView):
    model = models.Creation


class CreationCreateView(CreateView):
    model = models.Creation


class CreationDetailView(DetailView):
    model = models.Creation


class CreationUpdateView(UpdateView):
    model = models.Creation


class DataFormatListView(ListView):
    model = models.DataFormat


class DataFormatCreateView(CreateView):
    model = models.DataFormat


class DataFormatDetailView(DetailView):
    model = models.DataFormat


class DataFormatUpdateView(UpdateView):
    model = models.DataFormat


class DocumentListView(ListView):
    model = models.Document


class DocumentationListView(ListView):
    model = models.Documentation


class DomainListView(ListView):
    model = models.Domain


class DocumentCreateView(CreateView):
    model = models.Document


class DocumentDetailView(DetailView):
    model = models.Document


class DocumentUpdateView(UpdateView):
    model = models.Document


class FunctionInfoListView(ListView):
    model = models.FunctionInfo


class FunctionInfoCreateView(CreateView):
    model = models.FunctionInfo


class FunctionInfoDetailView(DetailView):
    model = models.FunctionInfo


class FunctionInfoUpdateView(UpdateView):
    model = models.FunctionInfo


class GeographicClassificationListView(ListView):
    model = models.GeographicClassification


class GeographicClassificationCreateView(CreateView):
    model = models.GeographicClassification


class GeographicClassificationDetailView(DetailView):
    model = models.GeographicClassification


class GeographicClassificationUpdateView(UpdateView):
    model = models.GeographicClassification


class GroupIdentifierListView(ListView):
    model = models.GroupIdentifier


class GroupIdentifierCreateView(CreateView):
    model = models.GroupIdentifier


class GroupIdentifierDetailView(DetailView):
    model = models.GroupIdentifier


class GroupIdentifierUpdateView(UpdateView):
    model = models.GroupIdentifier


class IdentificationListView(ListView):
    model = models.Identification


class IdentificationCreateView(CreateView):
    model = models.Identification


class IdentificationDetailView(DetailView):
    model = models.Identification


class IdentificationUpdateView(UpdateView):
    model = models.Identification


class LanguageListView(ListView):
    model = models.Language


class LanguageCreateView(CreateView):
    model = models.Language


class LanguageDetailView(DetailView):
    model = models.Language


class LanguageUpdateView(UpdateView):
    model = models.Language


class LanguageInfoListView(ListView):
    model = models.LanguageInfo


class LanguageInfoCreateView(CreateView):
    model = models.LanguageInfo


class LanguageInfoDetailView(DetailView):
    model = models.LanguageInfo


class LanguageInfoUpdateView(UpdateView):
    model = models.LanguageInfo


class LanguageVarietyListView(ListView):
    model = models.LanguageVariety


class LanguageVarietyCreateView(CreateView):
    model = models.LanguageVariety


class LanguageVarietyDetailView(DetailView):
    model = models.LanguageVariety


class LanguageVarietyUpdateView(UpdateView):
    model = models.LanguageVariety


class LicenceListView(ListView):
    model = models.Licence


class LicenceCreateView(CreateView):
    model = models.Licence


class LicenceDetailView(DetailView):
    model = models.Licence


class LicenceUpdateView(UpdateView):
    model = models.Licence


class LicenceIdentifierListView(ListView):
    model = models.LicenceIdentifier


class LicenceIdentifierCreateView(CreateView):
    model = models.LicenceIdentifier


class LicenceIdentifierDetailView(DetailView):
    model = models.LicenceIdentifier


class LicenceIdentifierUpdateView(UpdateView):
    model = models.LicenceIdentifier


class LingualityListView(ListView):
    model = models.Linguality


class LingualityCreateView(CreateView):
    model = models.Linguality


class LingualityDetailView(DetailView):
    model = models.Linguality


class LingualityUpdateView(UpdateView):
    model = models.Linguality


class MailingListListView(ListView):
    model = models.MailingList


class MailingListCreateView(CreateView):
    model = models.MailingList


class MailingListDetailView(DetailView):
    model = models.MailingList


class MailingListUpdateView(UpdateView):
    model = models.MailingList


class MediaPartListView(ListView):
    model = models.MediaPart


class MediaPartCreateView(CreateView):
    model = models.MediaPart


class MediaPartDetailView(DetailView):
    model = models.MediaPart


class MediaPartUpdateView(UpdateView):
    model = models.MediaPart


class MediaTechnicalInfoListView(ListView):
    model = models.MediaTechnicalInfo


class MediaTechnicalInfoCreateView(CreateView):
    model = models.MediaTechnicalInfo


class MediaTechnicalInfoDetailView(DetailView):
    model = models.MediaTechnicalInfo


class MediaTechnicalInfoUpdateView(UpdateView):
    model = models.MediaTechnicalInfo


class MetadataHeaderListView(ListView):
    model = models.MetadataHeader


class MetadataHeaderCreateView(CreateView):
    model = models.MetadataHeader


class MetadataHeaderDetailView(DetailView):
    model = models.MetadataHeader


class MetadataHeaderUpdateView(UpdateView):
    model = models.MetadataHeader


class MetadataIdentifierListView(ListView):
    model = models.MetadataIdentifier


class MetadataIdentifierCreateView(CreateView):
    model = models.MetadataIdentifier


class MetadataIdentifierDetailView(DetailView):
    model = models.MetadataIdentifier


class MetadataIdentifierUpdateView(UpdateView):
    model = models.MetadataIdentifier


class OrganizationIdentifierListView(ListView):
    model = models.OrganizationIdentifier


class OrganizationIdentifierCreateView(CreateView):
    model = models.OrganizationIdentifier


class OrganizationIdentifierDetailView(DetailView):
    model = models.OrganizationIdentifier


class OrganizationIdentifierUpdateView(UpdateView):
    model = models.OrganizationIdentifier


class PersonListView(ListView):
    model = models.Person


class PersonCreateView(CreateView):
    model = models.Person


class PersonDetailView(DetailView):
    model = models.Person


class PersonIdentifierListView(ListView):
    model = models.PersonIdentifier


class PersonIdentifierCreateView(CreateView):
    model = models.PersonIdentifier


class PersonIdentifierDetailView(DetailView):
    model = models.PersonIdentifier


class PersonIdentifierUpdateView(UpdateView):
    model = models.PersonIdentifier


class PhysicalResourceListView(ListView):
    model = models.PhysicalResource


class PhysicalResourceCreateView(CreateView):
    model = models.PhysicalResource


class PhysicalResourceDetailView(DetailView):
    model = models.PhysicalResource


class PhysicalResourceUpdateView(UpdateView):
    model = models.PhysicalResource


class ModalityListView(ListView):
    model = models.Modality


class ModalityCreateView(CreateView):
    model = models.Modality


class ModalityDetailView(DetailView):
    model = models.Modality


class ModalityUpdateView(UpdateView):
    model = models.Modality


class ProcessingResourceListView(ListView):
    model = models.ProcessingResource


class ProcessingResourceCreateView(CreateView):
    model = models.ProcessingResource


class ProcessingResourceDetailView(DetailView):
    model = models.ProcessingResource


class ProcessingResourceUpdateView(UpdateView):
    model = models.ProcessingResource


class PublicationIdentifierListView(ListView):
    model = models.PublicationIdentifier


class PublicationIdentifierCreateView(CreateView):
    model = models.PublicationIdentifier


class PublicationIdentifierDetailView(DetailView):
    model = models.PublicationIdentifier


class PublicationIdentifierUpdateView(UpdateView):
    model = models.PublicationIdentifier


class RegisterListView(ListView):
    model = models.Register


class RegisterCreateView(CreateView):
    model = models.Register


class RegisterDetailView(DetailView):
    model = models.Register


class RegisterUpdateView(UpdateView):
    model = models.Register


class RepositoryListView(ListView):
    model = models.Repository


class RelationListView(ListView):
    model = models.Relation


class RepositoryCreateView(CreateView):
    model = models.Repository


class RepositoryDetailView(DetailView):
    model = models.Repository


class RepositoryUpdateView(UpdateView):
    model = models.Repository


class RepositoryIdentifierListView(ListView):
    model = models.RepositoryIdentifier


class RepositoryIdentifierCreateView(CreateView):
    model = models.RepositoryIdentifier


class RepositoryIdentifierDetailView(DetailView):
    model = models.RepositoryIdentifier


class RepositoryIdentifierUpdateView(UpdateView):
    model = models.RepositoryIdentifier


class ResourceListView(ListView):
    model = models.Resource


class ResourceCreateView(CreateView):
    model = models.Resource


class ResourceDetailView(DetailView):
    model = models.Resource


class ResourceUpdateView(UpdateView):
    model = models.Resource


class ResourceIdentifierListView(ListView):
    model = models.ResourceIdentifier


class ResourceIdentifierCreateView(CreateView):
    model = models.ResourceIdentifier


class ResourceIdentifierDetailView(DetailView):
    model = models.ResourceIdentifier


class ResourceIdentifierUpdateView(UpdateView):
    model = models.ResourceIdentifier


class ResourceRightsListView(ListView):
    model = models.ResourceRights


class ResourceRightsCreateView(CreateView):
    model = models.ResourceRights


class ResourceRightsDetailView(DetailView):
    model = models.ResourceRights


class ResourceRightsUpdateView(UpdateView):
    model = models.ResourceRights


class SizeListView(ListView):
    model = models.Size


class SizeCreateView(CreateView):
    model = models.Size


class SizeDetailView(DetailView):
    model = models.Size


class SizeUpdateView(UpdateView):
    model = models.Size


class TextFormatListView(ListView):
    model = models.TextFormat


class TextFormatCreateView(CreateView):
    model = models.TextFormat


class TextFormatDetailView(DetailView):
    model = models.TextFormat


class TextFormatUpdateView(UpdateView):
    model = models.TextFormat


class TextPartListView(ListView):
    model = models.TextPart


class TextPartCreateView(CreateView):
    model = models.TextPart


class TextPartDetailView(DetailView):
    model = models.TextPart


class TextPartUpdateView(UpdateView):
    model = models.TextPart


class TimeClassificationListView(ListView):
    model = models.TimeClassification


class TimeClassificationCreateView(CreateView):
    model = models.TimeClassification


class TimeClassificationDetailView(DetailView):
    model = models.TimeClassification


class TimeClassificationUpdateView(UpdateView):
    model = models.TimeClassification


class SourceOfMetadataRecordListView(ListView):
    model = models.SourceOfMetadataRecord


class SourceOfMetadataRecordCreateView(CreateView):
    model = models.SourceOfMetadataRecord


class SourceOfMetadataRecordDetailView(DetailView):
    model = models.SourceOfMetadataRecord


class SourceOfMetadataRecordUpdateView(UpdateView):
    model = models.SourceOfMetadataRecord


class SubjectListView(ListView):
    model = models.Subject


class SubjectCreateView(CreateView):
    model = models.Subject


class SubjectDetailView(DetailView):
    model = models.Subject


class SubjectUpdateView(UpdateView):
    model = models.Subject


class TextClassificationListView(ListView):
    model = models.TextClassification


class TextClassificationCreateView(CreateView):
    model = models.TextClassification


class TextClassificationDetailView(DetailView):
    model = models.TextClassification


class TextClassificationUpdateView(UpdateView):
    model = models.TextClassification


class TextGenreListView(ListView):
    model = models.TextGenre


class TextGenreCreateView(CreateView):
    model = models.TextGenre


class TextGenreDetailView(DetailView):
    model = models.TextGenre


class TextGenreUpdateView(UpdateView):
    model = models.TextGenre


class TextTypeListView(ListView):
    model = models.TextType


class TextTypeCreateView(CreateView):
    model = models.TextType


class TextTypeDetailView(DetailView):
    model = models.TextType


class TextTypeUpdateView(UpdateView):
    model = models.TextType


class VersionListView(ListView):
    model = models.Version


class VersionCreateView(CreateView):
    model = models.Version


class VersionDetailView(DetailView):
    model = models.Version


class VersionUpdateView(UpdateView):
    model = models.Version
