from collections import defaultdict

from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_polymorphic.serializers import PolymorphicSerializer

from repository import models


class ResourceIdentifierSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ResourceIdentifier
        exclude = ('identification',)


class ResourceSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.Resource
        exclude = ('component_dependency',)

    identifiers = ResourceIdentifierSerializer(many=True, allow_null=False)

    # def validate(self, data):
    #     """
    #     Check for mutual exclusion
    #     """
    #     if data.get('name') and data.get('identifiers'):
    #         raise ValidationError("You can define either name or identifiers")
    #     elif not data.get('name') and not data.get('identifiers'):
    #         raise ValidationError("You must define either name or identifiers")
    #     return data


class AnnotationTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AnnotationType
        exclude = ('annotation', 'processing_resource')


class PublicationIdentifierSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PublicationIdentifier
        fields = '__all__'


class DocumentSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.Document
        exclude = ('annotation',)

    publication_identifiers = PublicationIdentifierSerializer(many=True, allow_null=True)


class DateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Date
        fields = '__all__'


class DateRangeTypeSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.DateRangeType
        fields = '__all__'

    start_date = DateSerializer()
    end_date = DateSerializer()


class DateCombinationSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.DateCombination
        fields = '__all__'

    date = DateSerializer(allow_null=True)
    date_range = DateRangeTypeSerializer(allow_null=True)

    def validate(self, data):
        """
        Check for mutual exclusion
        """
        if data.get('date') and data.get('date_range'):
            raise ValidationError("You can define either 'date' or 'date range'")
        elif not data.get('date') and not data.get('date_range'):
            raise ValidationError("You must define either 'date' or 'date range'")
        return data


class PersonIdentifierSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.PersonIdentifier
        fields = '__all__'


class PersonSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.Person
        exclude = ('polymorphic_ctype',)

    identifiers = PersonIdentifierSerializer(many=True)

    # When creating/updating Actor directly
    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super(PersonSerializer, self).create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super(PersonSerializer, self).update(instance, validated_data)
        return instance


class OrganizationIdentifierSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.OrganizationIdentifier
        fields = '__all__'


class OrganizationSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.Organization
        exclude = ('polymorphic_ctype',)

    identifiers = OrganizationIdentifierSerializer(many=True)

    # When creating/updating Actor directly
    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance


class GroupIdentifierSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.GroupIdentifier
        fields = '__all__'


class GroupSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.Group
        exclude = ('polymorphic_ctype',)

    identifiers = GroupIdentifierSerializer(many=True)

    # When creating/updating Actor directly
    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance


class ActorSerializer(PolymorphicSerializer, WritableNestedModelSerializer):
    resource_type_field_name = 'type'

    class Meta:
        model = models.Actor
        fields = '__all__'

    model_serializer_mapping = {
        models.Organization: OrganizationSerializer,
        models.Person: PersonSerializer,
        models.Group: GroupSerializer,
    }

    def to_resource_type(self, model_or_instance):
        return model_or_instance._meta.object_name.lower()


class SizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Size
        exclude = ('text_dataset_technical_info',)


class AnnotationSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.Annotation
        exclude = ('text_part',)

    annotation_types = AnnotationTypeSerializer(many=True)
    type_system = ResourceSerializer(allow_null=True)
    annotation_schema = ResourceSerializer(allow_null=True)
    annotation_resource = ResourceSerializer(allow_null=True)
    guidelines_documented_in = DocumentSerializer(many=True, allow_null=False)
    is_annotated_by = ResourceSerializer(many=True, allow_null=False)
    annotation_date = DateCombinationSerializer(allow_null=True)
    annotators = ActorSerializer(many=True, allow_null=False)
    size = SizeSerializer(allow_null=True)


class CharacterEncodingSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.CharacterEncoding
        exclude = ('text_dataset_technical_info',)

    size = SizeSerializer(allow_null=True)


class IdentificationSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.Identification
        fields = '__all__'

    resource_identifiers = ResourceIdentifierSerializer(many=True, allow_empty=False)


class VersionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Version
        fields = '__all__'


class MailingListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MailingList
        fields = '__all__'


class ContactSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.Contact
        fields = '__all__'

    contact_persons = PersonSerializer(many=True, allow_null=False)

    contact_organizations = OrganizationSerializer(many=True, allow_null=False)

    contact_groups = GroupSerializer(many=True, allow_null=False)

    mailing_lists = MailingListSerializer(many=True, allow_null=False)


class DataFormatSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DataFormat
        exclude = ('processing_resource',)


class TextFormatSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.TextFormat
        exclude = ('text_dataset_technical_info',)

    data_format = DataFormatSerializer()
    size = SizeSerializer()


class TextDatasetTechnicalInfoSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.TextDatasetTechnicalInfo
        exclude = ('polymorphic_ctype', 'dataset_distribution')

    sizes = SizeSerializer(many=True, required=True)
    text_formats = TextFormatSerializer(many=True, required=True)
    character_encodings = CharacterEncodingSerializer(many=True)

    # When creating/updating MediaTechnicalInfo directly
    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance


class MediaTechnicalInfoSerializer(PolymorphicSerializer, WritableNestedModelSerializer):
    class Meta:
        model = models.MediaTechnicalInfo
        exclude = ('dataset_distribution',)

    resource_type_field_name = 'media_type'

    model_serializer_mapping = {
        models.TextDatasetTechnicalInfo: TextDatasetTechnicalInfoSerializer
    }

    def to_resource_type(self, model_or_instance):
        return model_or_instance._meta.object_name.lower()


class LicenceIdentifierSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.LicenceIdentifier
        fields = '__all__'


class LicenceSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.Licence
        fields = '__all__'

    identifiers = LicenceIdentifierSerializer(many=True, allow_null=False)


class DistributionRightsSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.DistributionRights
        fields = '__all__'

    licences = LicenceSerializer(many=True, required=True, allow_null=False)


class DatasetDistributionSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.DatasetDistribution
        fields = '__all__'

    technical_parts = MediaTechnicalInfoSerializer(many=True, allow_null=False)
    distribution_rights = DistributionRightsSerializer()


class LingualitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Linguality
        fields = '__all__'


class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Language
        exclude = ('metadata_header', 'processing_resource')


class LanguageVarietySerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.LanguageVariety
        exclude = ('language_info',)

    size = SizeSerializer(allow_null=True)


class LanguageInfoSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.LanguageInfo
        exclude = ('corpus',)

    language = LanguageSerializer()
    size = SizeSerializer(allow_null=True)
    language_varieties = LanguageVarietySerializer(many=True, allow_null=False)


class TimeClassificationSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.TimeClassification
        exclude = ('corpus',)

    size = SizeSerializer(allow_null=True)


class GeographicClassificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.GeographicClassification
        exclude = ('corpus',)

    size = SizeSerializer(allow_null=True)


class RelationSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.Relation
        exclude = ('common_info',)

    related_resource = ResourceSerializer()


class CreationSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.Creation
        fields = '__all__'

    original_sources = RelationSerializer(many=True, allow_null=False)
    created_by = RelationSerializer(many=True, allow_null=False)


class ModalitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Modality
        fields = '__all__'

    size = SizeSerializer(allow_null=True)


class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Register
        fields = '__all__'


class TextGenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TextGenre
        fields = '__all__'


class TextTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TextType
        fields = '__all__'


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Subject
        fields = '__all__'


class TextClassificationSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.TextClassification
        exclude = ('text_part',)

    text_genre = TextGenreSerializer(allow_null=True)
    text_type = TextTypeSerializer(allow_null=True)
    register = RegisterSerializer(allow_null=True)
    subject = SubjectSerializer(allow_null=True)

    size = SizeSerializer(allow_null=True)


class TextPartSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.TextPart
        exclude = ('polymorphic_ctype', 'corpus')

    creation = CreationSerializer(allow_null=True)
    modality = ModalitySerializer(allow_null=True)
    text_classifications = TextClassificationSerializer(many=True, allow_null=False)
    annotations = AnnotationSerializer(many=True, allow_null=False)

    # link_to_other_media TODO

    # When creating/updating MediaPart directly
    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance


class MediaPartSerializer(PolymorphicSerializer, WritableNestedModelSerializer):
    resource_type_field_name = 'media_type'

    class Meta:
        model = models.MediaPart
        exclude = ('corpus',)

    model_serializer_mapping = {
        models.TextPart: TextPartSerializer,
    }

    def to_resource_type(self, model_or_instance):
        return model_or_instance._meta.object_name.lower()


class CorpusSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.Corpus
        exclude = ('polymorphic_ctype',)

    dataset_distribution = DatasetDistributionSerializer()

    linguality = LingualitySerializer()

    languages = LanguageInfoSerializer(many=True)

    time_classifications = TimeClassificationSerializer(many=True, allow_null=False)

    geographic_classifications = GeographicClassificationSerializer(many=True, allow_null=False)

    corpus_parts = MediaPartSerializer(many=True, allow_null=False, required=True)

    # When creating/updating LanguageResource directly
    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance


class FunctionInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.FunctionInfo
        fields = '__all__'


class ComponentDistributionSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.ComponentDistribution
        exclude = ('tool_service',)

    distribution_rights = DistributionRightsSerializer()


class ProcessingResourceSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.ProcessingResource
        exclude = ('tool_service_input', 'tool_service_output')

    data_formats = DataFormatSerializer(many=True, allow_null=False)
    languages = LanguageSerializer(many=True, allow_null=False)
    annotation_types = AnnotationTypeSerializer(many=True, allow_null=False)
    type_system = ResourceSerializer(allow_null=True)
    annotation_schema = ResourceSerializer(allow_null=True)
    annotation_resource = ResourceSerializer(allow_null=True)


class ComponentDependencySerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.ComponentDependency
        exclude = ('tool_service',)

    type_system = ResourceSerializer(allow_null=True)
    annotation_schema = ResourceSerializer(allow_null=True)
    annotation_resources = ResourceSerializer(many=True, allow_null=False)
    ml_model = ResourceSerializer(allow_null=True)


class ToolServiceSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.ToolService
        exclude = ('polymorphic_ctype',)

    function_info = FunctionInfoSerializer()
    distributions = ComponentDistributionSerializer(many=True, allow_null=False)
    input_content_resources = ProcessingResourceSerializer(many=True, allow_null=False)
    output_resources = ProcessingResourceSerializer(many=True, allow_null=False)
    dependencies = ComponentDependencySerializer(many=True, allow_null=False)

    # When creating/updating LanguageResource directly
    def create(self, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        self._save_kwargs = defaultdict(dict)
        instance = super().update(instance, validated_data)
        return instance


class ToolServiceCreationSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.ToolServiceCreation
        fields = '__all__'

    original_sources = ResourceSerializer(many=True, allow_null=False)


class LanguageResourceSerializer(PolymorphicSerializer, WritableNestedModelSerializer):
    resource_type_field_name = 'resource_type'

    class Meta:
        model = models.LanguageResource
        fields = '__all__'

    model_serializer_mapping = {
        models.Corpus: CorpusSerializer,
        models.ToolService: ToolServiceSerializer
    }

    def to_resource_type(self, model_or_instance):
        return model_or_instance._meta.object_name.lower()


class ResourceRightsSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.ResourceRights
        fields = '__all__'

    rights_holders = ActorSerializer(many=True, allow_null=False)


class DocumentationSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.Documentation
        exclude = ('common_info',)

    publication_identifiers = PublicationIdentifierSerializer(many=True, allow_null=True)

    # check for mutual exclusion
    def validate(self, data):
        """
        Check for mutual exclusion
        """
        fields_inserted = 0

        for k in data:
            if data.get(k):
                fields_inserted += 1

        if fields_inserted >= 3:
            raise ValidationError("You can define either 'Document Bibtex', 'Bibliographic Data' or "
                                  "'publication identifiers'")
        elif not data.get('document_bibtex') \
                and not data.get('documentation_bibliographic_data') \
                and not data.get('publication_identifiers'):
            raise ValidationError("You must define 'Document Bibtex', 'Bibliographic Data' or "
                                  "'Publication Identifiers'")
        return data


class DomainSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Domain
        fields = '__all__'


class CommonInfoSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.CommonInfo
        fields = '__all__'

    identification = IdentificationSerializer()
    version = VersionSerializer()
    contact = ContactSerializer()
    resource_rights = ResourceRightsSerializer()
    documentations = DocumentationSerializer(many=True, allow_null=False)
    domains = DomainSerializer(many=True, allow_null=False)
    relations = RelationSerializer(many=True, allow_null=False)


class MetadataIdentifierSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.MetadataIdentifier
        fields = '__all__'


class RepositoryIdentifierSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RepositoryIdentifier
        fields = '__all__'


class RepositorySerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.Repository
        fields = '__all__'

    identifiers = RepositoryIdentifierSerializer(
        help_text=Meta.model._meta.get_field('identifiers').help_text,
        many=True,
        allow_null=False)


class SourceOfMetadataRecordSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.SourceOfMetadataRecord
        fields = '__all__'

    collected_from = RepositorySerializer()


class MetadataHeaderSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.MetadataHeader
        fields = '__all__'

    metadata_record_identifier = MetadataIdentifierSerializer(
        help_text=Meta.model._meta.get_field('metadata_record_identifier').help_text
    )

    metadata_creators = PersonSerializer(many=True, allow_null=False)

    source_of_metadata_record = SourceOfMetadataRecordSerializer(allow_null=True)

    metadata_languages = LanguageSerializer(many=True, allow_null=False)


class PhysicalResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PhysicalResource
        fields = '__all__'


class ResourceMetadataRecordSerializer(WritableNestedModelSerializer):
    class Meta:
        model = models.ResourceMetadataRecord
        fields = '__all__'

    metadata_header = MetadataHeaderSerializer()
    common_info = CommonInfoSerializer()
    language_resource = LanguageResourceSerializer()
